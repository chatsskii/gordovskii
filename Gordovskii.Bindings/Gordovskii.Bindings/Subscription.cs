﻿using System;

namespace Gordovskii.Bindings
{
    internal class Subscription : IDisposable
    {
        private readonly Action _unsubscribe;

        public Subscription(Action unsubscribe)
        {
            _unsubscribe = unsubscribe ?? throw new ArgumentNullException(nameof(unsubscribe));
        }

        public bool IsDisposed { get; private set; } = false;

        public void Dispose()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(nameof(Subscription));

            _unsubscribe();
            IsDisposed = true;
        }
    }


}
