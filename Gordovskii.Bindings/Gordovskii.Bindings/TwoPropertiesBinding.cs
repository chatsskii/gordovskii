﻿using System;
using System.ComponentModel;

namespace Gordovskii.Bindings
{
    public sealed class TwoPropertiesBinding 
    {
        private readonly string _sourcePropertyName;
        private readonly string _targetPropertyName;

        internal TwoPropertiesBinding(string sourcePropertyName, string targetPropertyName)
        {
            _sourcePropertyName = sourcePropertyName ?? throw new ArgumentNullException(nameof(sourcePropertyName));
            _targetPropertyName = targetPropertyName ?? throw new ArgumentNullException(nameof(targetPropertyName));
        }

        public IDisposable SubscribeOneWay(INotifyPropertyChanged source, object target)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (target == null)
                throw new ArgumentNullException(nameof(target));

            PropertyChangedEventHandler sourcePropertyChanged = CreatePropertyBindingHandler(source, _sourcePropertyName, target, _targetPropertyName);

            source.PropertyChanged += sourcePropertyChanged;

            SetValue(source, _sourcePropertyName, target, _targetPropertyName);
            return new Subscription(() => source.PropertyChanged -= sourcePropertyChanged);
        }

        public IDisposable SubscribeTwoWay(INotifyPropertyChanged source, INotifyPropertyChanged target)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (target == null)
                throw new ArgumentNullException(nameof(target));

            PropertyChangedEventHandler sourcePropertyChanged = CreatePropertyBindingHandler(source, _sourcePropertyName, target, _targetPropertyName);
            PropertyChangedEventHandler targetPropertyChanged = CreatePropertyBindingHandler(target, _targetPropertyName, source, _sourcePropertyName);

            source.PropertyChanged += sourcePropertyChanged;
            target.PropertyChanged += targetPropertyChanged;

            SetValue(source, _sourcePropertyName, target, _targetPropertyName);
            return new Subscription(() => 
            {
                source.PropertyChanged -= sourcePropertyChanged;
                target.PropertyChanged -= targetPropertyChanged;
            });
        }

        private PropertyChangedEventHandler CreatePropertyBindingHandler(object source, string sourcePropertyName, object target, string targetPropertyName)
        {
            return (_, args) =>
            {
                if (args.PropertyName != _sourcePropertyName)
                    return;

                SetValue(source, sourcePropertyName, target, targetPropertyName);
            };
        }

        private void SetValue(object source, string sourcePropertyName, object target, string targetPropertyName)
        {
            var sourceProperty = source.GetType().GetProperty(sourcePropertyName);
            var targetProperty = target.GetType().GetProperty(targetPropertyName);

            if (sourceProperty != null && targetProperty != null)
                targetProperty.SetValue(target, sourceProperty.GetValue(source));
        }
    }
}
