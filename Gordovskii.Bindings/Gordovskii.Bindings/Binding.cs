﻿using System;
using System.ComponentModel;

namespace Gordovskii.Bindings
{
    public sealed class Binding : ActionBinding
    {
        private Binding() { }

        public static Binding AnyProperty() => new Binding();
        public static PropertyBinding Property(string propertyName) => new PropertyBinding(propertyName);

        protected override PropertyChangedEventHandler CreateActionHandler(Action action)
            => (x, y) => action();

        protected override PropertyChangedEventHandler CreateActionHandler<T>(Action<T> action, T source)
            => (x, y) => action(source);
    }
}
