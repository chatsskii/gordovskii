﻿using System;
using System.ComponentModel;

namespace Gordovskii.Bindings
{
    public abstract class ActionBinding
    {
        public IDisposable SubscribeAction(INotifyPropertyChanged source, Action action)
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (action == null)
                throw new ArgumentNullException(nameof(action));

            var handler = CreateActionHandler(action);

            source.PropertyChanged += handler;
            return new Subscription(() => source.PropertyChanged -= handler);
        }
        public IDisposable SubscribeAction<T>(T source, Action<T> action) where T : INotifyPropertyChanged
        {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (action == null)
                throw new ArgumentNullException(nameof(action));

            var handler = CreateActionHandler(action, source);

            source.PropertyChanged += handler;
            return new Subscription(() => source.PropertyChanged -= handler);
        }

        protected abstract PropertyChangedEventHandler CreateActionHandler(Action action);
        protected abstract PropertyChangedEventHandler CreateActionHandler<T>(Action<T> action, T source) where T : INotifyPropertyChanged;
    }
}
