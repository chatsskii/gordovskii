﻿using System;
using System.ComponentModel;

namespace Gordovskii.Bindings
{

    public sealed class PropertyBinding : ActionBinding
    {
        private readonly string _propertyName;

        internal PropertyBinding(string propertyName)
        {
            _propertyName = propertyName ?? throw new ArgumentNullException(nameof(propertyName));
        }

        public TwoPropertiesBinding ToProperty(string targetProperty = null) => new TwoPropertiesBinding(_propertyName, targetProperty ?? _propertyName);

        protected override PropertyChangedEventHandler CreateActionHandler(Action action)
        {
            return (_, args) =>
            {
                if (args.PropertyName == _propertyName)
                    action();
            };
        }

        protected override PropertyChangedEventHandler CreateActionHandler<T>(Action<T> action, T source)
        {
            return (_, args) =>
            {
                if (args.PropertyName == _propertyName)
                    action(source);
            };
        }
    }
}
