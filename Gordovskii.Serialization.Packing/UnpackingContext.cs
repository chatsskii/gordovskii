﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Gordovskii.Serialization.Packing
{
    internal class UnpackingContext
    {
        private const string NOT_REGISTERED_UNPACK_METHOD_NAME = "NOT_REGISTERED_UNPACK";

        private readonly Dictionary<long, PackedObject> _packedObjects;
        private readonly Dictionary<long, IPackableObject> _unpackedObjects = new Dictionary<long, IPackableObject>();

        private UnpackingContext(IEnumerable<PackedObject> packedObjects)
        {
            _packedObjects = new Dictionary<long, PackedObject>();
            foreach (var packedObject in packedObjects)
                _packedObjects.Add(packedObject.Id, packedObject);
        }
        public static UnpackingContext FromPack(SerializablePack pack)
        {
            if (pack == null)
                throw new ArgumentNullException(nameof(pack));

            UnpackingContext unpackingContext = new UnpackingContext(pack.PackedObjects);
            return unpackingContext;
        }

        public TPackable GetFirst<TPackable>() where TPackable : IPackableObject
        {
            var packedObject = _packedObjects.Values.First(x => x.GetObjectType() == typeof(TPackable));
            return EnsureUnpackedObject<TPackable>(packedObject.Id);
        }
        private TPackable EnsureUnpackedObject<TPackable>(long id) where TPackable : IPackableObject
        {
            if (id == 0)
                return default;

            if (_unpackedObjects.TryGetValue(id, out IPackableObject unpackedObject))
                return (TPackable)unpackedObject;

            var packedObject = _packedObjects[id];

            if (typeof(PackableObject).IsAssignableFrom(packedObject.GetObjectType()))
            {
                var constructor = packedObject.GetObjectType().GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public, null, new Type[] { typeof(IUnpackingProvider) }, null);
                return (TPackable)constructor.Invoke(new object[] { new Provider(this, packedObject) });
            }

            // Если по какой-то причине объект не наследуется от базового класса.
            // Например он наследуется от другого класса из чужой библиотеки, 
            // то попробуем вызвать у него статический метод фабрику, который создаст класс.
            // В таком случае могут возникнуть ошибки, так-как объект регистрируется после конструктора,
            // что может вызвать ошибки, если происходит циклическая ссылка.
            // Необходимо использовать только в крайнем случае, на свой страх и риск.
            try
            {
                var method = packedObject
                    .GetObjectType()
                    .GetMethod(NOT_REGISTERED_UNPACK_METHOD_NAME, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static, null, new Type[] { typeof(IUnpackingProvider) }, null);

                var unregisteredObject = (TPackable)method.Invoke(null, new object[] { new Provider(this, packedObject) });

                // Регистрация уже после создания объекта.
                Register(packedObject, unregisteredObject);
                return unregisteredObject;
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        private void Register(PackedObject packedObject, IPackableObject obj)
            => _unpackedObjects.Add(packedObject.Id, obj);

        private sealed class Provider : IUnpackingProvider, IRegistrationProvider
        {
            private readonly UnpackingContext _context;
            private readonly PackedObject _packedObject;

            public Provider(UnpackingContext context, PackedObject packedObject)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _packedObject = packedObject ?? throw new ArgumentNullException(nameof(packedObject));
            }

            public void Register(IPackableObject obj)
                => _context.Register(_packedObject, obj);

            public TConvertable UnpackValue<TConvertable>(string key) where TConvertable : IConvertible
                => (TConvertable)Convert.ChangeType(FindValue(key).ValueString, typeof(TConvertable));
            public TValue UnpackValue<TValue>(string key, Func<string, TValue> convert)
                => convert(UnpackValue<string>(key));

            public IEnumerable<TConvertable> UnpackValues<TConvertable>(string key) where TConvertable : IConvertible
            {
                string valueString = FindValue(key).ValueString;
                return valueString
                    .Substring(1, valueString.Length - 2)
                    .Split(new string[] { "','" }, StringSplitOptions.None)
                    .Select(v => (TConvertable)Convert.ChangeType(v, typeof(TConvertable)));
            }
            public IEnumerable<TValue> UnpackValues<TValue>(string key, Func<string, TValue> convert)
                => UnpackValues<string>(key).Select(v => convert(v));

            public TPackable UnpackReference<TPackable>(string key) where TPackable : IPackableObject
                => _context.EnsureUnpackedObject<TPackable>(long.Parse(FindValue(key).ValueString));
            public IEnumerable<TPackable> UnpackReferences<TPackable>(string key) where TPackable : IPackableObject
            {
                var valuesString = FindValue(key).ValueString;
                if (valuesString == string.Empty)
                    return Enumerable.Empty<TPackable>();

                return valuesString.Split(',').Select(v => _context.EnsureUnpackedObject<TPackable>(long.Parse(v)));
            }

            private PackedValue FindValue(string key) => _packedObject.Values.First(v => v.Key == key);
        }
    }
}
