﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Gordovskii.Serialization.Packing
{
    internal class PackingContext
    {
        private long _lastId = 0;
        private SerializablePack _serializablePack;

        private readonly Dictionary<IPackableObject, PackedObject> _packedObjects = new Dictionary<IPackableObject, PackedObject>();
        public SerializablePack Pack
        {
            get
            {
                if (_serializablePack != null)
                    return _serializablePack;

                _serializablePack = new SerializablePack()
                {
                    PackedObjects = new List<PackedObject>(_packedObjects.Values),
                };

                return _serializablePack;
            }
        }

        private PackingContext() { }
        public static PackingContext FromGraph<TPackableObject>(TPackableObject obj) where TPackableObject : IPackableObject
        {
            PackingContext packingContext = new PackingContext();
            packingContext.EnsurePackedObject(obj);
            return packingContext;
        }

        private long EnsurePackedObject(IPackableObject obj)
        {
            if (obj == null)
                return 0;

            if (_packedObjects.TryGetValue(obj, out PackedObject packedObject))
                return packedObject.Id;

            packedObject = new PackedObject()
            {
                Id = NextId(),
                ObjectType = obj.GetType().AssemblyQualifiedName,
            };

            _packedObjects.Add(obj, packedObject);
            obj.Pack(new Provider(this, packedObject));
            return packedObject.Id;
        }
        private long NextId() => ++_lastId;
        private sealed class Provider : IPackingProvider
        {
            private readonly PackingContext _context;
            private readonly PackedObject _packedObject;

            public Provider(PackingContext context, PackedObject packedObject)
            {
                _context = context ?? throw new ArgumentNullException(nameof(context));
                _packedObject = packedObject ?? throw new ArgumentNullException(nameof(packedObject));
            }

            public void PackValue(string key, IConvertible value)
                => _packedObject.Values.Add(new PackedValue() { Key = key, ValueString = value.ToString() });

            public void PackValue<TValue>(string key, TValue value, Func<TValue, IConvertible> convert)
                => PackValue(key, convert(value));


            public void PackValues(string key, IEnumerable<IConvertible> values)
                => _packedObject.Values.Add(new PackedValue() { Key = key, ValueString = string.Join(",", values.Select(v => $"'{v}'")) });
            public void PackValues<TValue>(string key, IEnumerable<TValue> values, Func<TValue, IConvertible> convert)
                => PackValues(key, values.Select(v => convert(v)));


            public void PackReference(string key, IPackableObject value)
                => _packedObject.Values.Add(new PackedValue() { Key = key, ValueString = _context.EnsurePackedObject(value).ToString() });

            public void PackReferences(string key, IEnumerable<IPackableObject> values)
                => _packedObject.Values.Add(new PackedValue() { Key = key, ValueString = string.Join(",", values.Select(v => _context.EnsurePackedObject(v).ToString())) });
        }
    }
}
