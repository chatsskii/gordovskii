﻿using System;
using System.Collections.Generic;

namespace Gordovskii.Serialization.Packing
{
    public interface IUnpackingProvider
    {
        TConvertable UnpackValue<TConvertable>(string key) where TConvertable : IConvertible;
        TValue UnpackValue<TValue>(string key, Func<string, TValue> convert);

        IEnumerable<TConvertable> UnpackValues<TConvertable>(string key) where TConvertable : IConvertible;
        IEnumerable<TValue> UnpackValues<TValue>(string key, Func<string, TValue> convert);

        TPackable UnpackReference<TPackable>(string key) where TPackable : IPackableObject;
        IEnumerable<TPackable> UnpackReferences<TPackable>(string key) where TPackable : IPackableObject;
    }
}
