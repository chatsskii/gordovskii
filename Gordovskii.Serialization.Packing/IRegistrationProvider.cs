﻿namespace Gordovskii.Serialization.Packing
{
    internal interface IRegistrationProvider
    {
        void Register(IPackableObject obj);
    }
}
