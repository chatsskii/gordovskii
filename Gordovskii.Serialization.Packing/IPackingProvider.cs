﻿using System;
using System.Collections.Generic;

namespace Gordovskii.Serialization.Packing
{
    public interface IPackingProvider
    {
        void PackValue(string key, IConvertible value);
        void PackValue<TValue>(string key, TValue value, Func<TValue, IConvertible> convert);

        void PackValues(string key, IEnumerable<IConvertible> values);
        void PackValues<TValue>(string key, IEnumerable<TValue> values, Func<TValue, IConvertible> convert);

        void PackReference(string key, IPackableObject value);
        void PackReferences(string key, IEnumerable<IPackableObject> values);
    }
}
