﻿namespace Gordovskii.Serialization.Packing
{
    public static class Packing
    {
        public static SerializablePack Pack<TPackable>(TPackable obj) where TPackable : IPackableObject
            => PackingContext.FromGraph(obj).Pack;

        public static TPackable Unpack<TPackable>(SerializablePack pack) where TPackable : IPackableObject
            => UnpackingContext.FromPack(pack).GetFirst<TPackable>();
    }
}
