﻿namespace Gordovskii.Serialization.Packing
{
    public abstract class PackableObject : IPackableObject
    {
        public PackableObject() { }
        protected PackableObject(IUnpackingProvider provider) => (provider as IRegistrationProvider).Register(this);
        public abstract void Pack(IPackingProvider provider);
    }
}
