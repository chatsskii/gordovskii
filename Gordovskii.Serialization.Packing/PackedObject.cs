﻿using System;
using System.Collections.Generic;

namespace Gordovskii.Serialization.Packing
{
    public sealed class PackedObject
    {
        public long Id { get; set; }
        public string ObjectType { get; set; }
        public List<PackedValue> Values { get; set; } = new List<PackedValue>();

        public Type GetObjectType() => Type.GetType(ObjectType);
    }
}
