﻿namespace Gordovskii.Serialization.Packing
{
    public interface IPackableObject
    {
        void Pack(IPackingProvider provider);
    }
}
