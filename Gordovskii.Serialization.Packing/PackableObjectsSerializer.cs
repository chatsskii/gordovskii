﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.Serialization.Packing
{
    public sealed class PackableObjectsSerializer<TPackable> : ISerializer<TPackable> where TPackable : IPackableObject
    {
        private readonly ISerializer _serializer;

        public PackableObjectsSerializer(ISerializer serializer)
        {
            _serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
        }

        public async Task<TPackable> DeserializeAsync(Stream stream, CancellationToken cancellationToken = default)
            => Packing.Unpack<TPackable>(await _serializer.DeserializeAsync<SerializablePack>(stream, cancellationToken).ConfigureAwait(false));

        public async Task SerializeAsync(Stream stream, TPackable value, CancellationToken cancellationToken = default)
            => await _serializer.SerializeAsync(stream, Packing.Pack(value), cancellationToken).ConfigureAwait(false);
    }
}
