﻿namespace Gordovskii.Serialization.Packing
{
    public sealed class PackedValue
    {
        public string Key { get; set; }
        public string ValueString { get; set; }
        public byte[] ValueBytes { get; set; }
    }
}
