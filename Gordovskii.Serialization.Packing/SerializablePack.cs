﻿using System.Collections.Generic;

namespace Gordovskii.Serialization.Packing
{
    public sealed class SerializablePack
    {
        public List<PackedObject> PackedObjects { get; set; } = new List<PackedObject>();
    }
}
