﻿using System;

namespace Gordovskii.Geodesy
{
    public struct Coordinates : IEquatable<Coordinates>
    {
        public double Latitude { get; }
        public double Longitude { get; }

        public Coordinates(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        #region IEquatable
        public override bool Equals(object obj) => obj is Coordinates coordinates && Equals(coordinates);
        public bool Equals(Coordinates other) => Latitude == other.Latitude && Longitude == other.Longitude;
        public override int GetHashCode()
        {
            int hashCode = -1416534245;
            hashCode = hashCode * -1521134295 + Latitude.GetHashCode();
            hashCode = hashCode * -1521134295 + Longitude.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(Coordinates left, Coordinates right) => left.Equals(right);
        public static bool operator !=(Coordinates left, Coordinates right) => !(left == right);
        #endregion
    }
}
