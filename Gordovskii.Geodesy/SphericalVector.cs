﻿using Gordovskii.MathEx;
using System;

namespace Gordovskii.Geodesy
{
    public struct SphericalVector
    {
        public double RadialDistance { get; }
        public double Azimuth { get; }

        public SphericalVector(double radialDistance, double azimuth)
        {
            RadialDistance = radialDistance;
            Azimuth = azimuth;
        }
    }
}
