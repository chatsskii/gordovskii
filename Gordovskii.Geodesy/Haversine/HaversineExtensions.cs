﻿using System;

namespace Gordovskii.Geodesy.Haversine
{
    public static class HaversineExtensions
    {
        // Формулы взял с сайта: https://www.movable-type.co.uk/scripts/latlong.html

        public static Coordinates Add(this Coordinates from, SphericalVector vector)
        {
            double latitude = Math.Sin(from.Latitude) * Math.Cos(vector.RadialDistance) + Math.Cos(from.Latitude) * Math.Sin(vector.RadialDistance) * Math.Cos(vector.Azimuth);
            double longitude = from.Longitude + Math.Atan2(Math.Sin(vector.RadialDistance) * Math.Sin(vector.Azimuth) , Math.Cos(from.Latitude) * Math.Cos(vector.RadialDistance) - Math.Sin(from.Latitude) * Math.Sin(vector.RadialDistance) * Math.Cos(vector.Azimuth));

            var to = new Coordinates(Math.Asin(latitude), longitude);
            return to;
        }

        public static double GetAzimuth(this Coordinates from, Coordinates to)
        {
            double delta = to.Longitude - from.Longitude;
            double y = Math.Sin(delta) * Math.Cos(to.Latitude);
            double x = Math.Cos(from.Latitude) * Math.Sin(to.Latitude) - Math.Sin(from.Latitude) * Math.Cos(to.Latitude) * Math.Cos(delta);
            double z = Math.Atan2(y, x);

            return (z + (2 * Math.PI)) % (2 * Math.PI);
        }

        public static double GetDistance(this Coordinates from, Coordinates to)
        {
            double deltaLat = to.Latitude - from.Latitude;
            double deltatLng = to.Longitude - from.Longitude;

            double a = Math.Sin(deltaLat / 2) * Math.Sin(deltaLat / 2) + Math.Cos(from.Latitude) * Math.Cos(to.Latitude) * Math.Sin(deltatLng / 2) * Math.Sin(deltatLng / 2);
            return 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
        }

        public static SphericalVector GetVector(this Coordinates from, Coordinates to) => new SphericalVector(GetDistance(from, to), GetAzimuth(from, to));
    }
}
