﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Gordovskii.Serialization.Packing.Tests
{
    public class PackingContextTests
    {
        [Test]
        public void Test()
        {
            var pack = Packing.Pack(new ClassA(12));
            var result = Packing.Unpack<ClassA>(pack);
        }
    }

    public sealed class ClassA : PackableObject
    {
        private int intField = 1;
        private string stringField = "stringField_A";
        private List<ClassB> classBs;
        private int[] intArray = new int[] { 1, 2, 3 };
        private Vector2 vector = new Vector2(1, 2);

        public int IntProperty { get; }

        public ClassA(int intProperty)
        {
            IntProperty = intProperty;
            var classB = new ClassB(this);
            classBs = new List<ClassB>() { classB, classB, classB };
        }

        private ClassA(IUnpackingProvider provider) : base(provider)
        {
            intField = provider.UnpackValue<int>(nameof(intField));
            stringField = provider.UnpackValue<string>(nameof(stringField));
            classBs = provider.UnpackReferences<ClassB>(nameof(classBs)).ToList();
            intArray = provider.UnpackValues<int>(nameof(intArray)).ToArray();
            IntProperty = provider.UnpackValue<int>(nameof(IntProperty));
            vector = provider.UnpackValue(nameof(vector), s => 
            {
                var values = s.Split(',').Select(v => float.Parse(v)).ToList();
                return new Vector2(values[0], values[1]);
            });
        }

        public override void Pack(IPackingProvider provider)
        {
            provider.PackValue(nameof(intField), intField);
            provider.PackValue(nameof(stringField), stringField);
            provider.PackReferences(nameof(classBs), classBs);
            provider.PackValues(nameof(intArray), intArray.Cast<IConvertible>());
            provider.PackValue(nameof(IntProperty), IntProperty);
            provider.PackValue(nameof(vector), vector, v => $"{v.X},{v.Y}");
        }

        public bool AllEqual => classBs.All(cb => classBs.First() == cb);


    }

    public sealed class ClassB : PackableObject
    {
        public event EventHandler Event;

        private static int globalId = 0;
        private int id = ++globalId;
        private int intField = 2;
        private string stringField = "stringField_B";
        private ClassC classC = new ClassC();
        private ClassA classA;



        public ClassB(ClassA classA)
        {
            Event += ClassB_Event;
            this.classA = classA;
        }

        public ClassB(IUnpackingProvider provider) : base(provider)
        {
            id = provider.UnpackValue<int>(nameof(id));
            intField = provider.UnpackValue<int>(nameof(intField));
            stringField = provider.UnpackValue<string>(nameof(stringField));
            classC = provider.UnpackReference<ClassC>(nameof(classC));
            classA = provider.UnpackReference<ClassA>(nameof(classA));
        }

        public override void Pack(IPackingProvider provider)
        {
            provider.PackValue(nameof(id), id);
            provider.PackValue(nameof(intField), intField);
            provider.PackValue(nameof(stringField), stringField);
            provider.PackReference(nameof(classC), classC);
            provider.PackReference(nameof(classA), classA);
        }

        private void ClassB_Event(object? sender, EventArgs e)
        {
            throw new NotImplementedException();
        }


    }

    public sealed class ClassC : PackableObject
    {
        private int intField = 3;
        private string stringField = "stringField_C";

        public ClassC() { }
        public ClassC(IUnpackingProvider provider) : base(provider)
        {
            intField = provider.UnpackValue<int>(nameof(intField));
            stringField = provider.UnpackValue<string>(nameof(stringField));
        }

        public override void Pack(IPackingProvider provider)
        {
            provider.PackValue(nameof(intField), intField);
            provider.PackValue(nameof(stringField), stringField);
        }
    }
}
