﻿using System.Text.Json;

namespace Gordovskii.Serialization.Json
{
    public sealed class JsonSerializer : ISerializer
    {
        private readonly JsonSerializerOptions? _options;

        public JsonSerializer(JsonSerializerOptions? options = null)
        {
            _options = options;
        }

        public async Task SerializeAsync<TValue>(Stream stream, TValue value, CancellationToken cancellationToken = default)
            => await System.Text.Json.JsonSerializer.SerializeAsync(stream, value, _options, cancellationToken).ConfigureAwait(false);

        public async Task<TValue?> DeserializeAsync<TValue>(Stream stream, CancellationToken cancellationToken = default)
            => await System.Text.Json.JsonSerializer.DeserializeAsync<TValue>(stream, _options, cancellationToken).ConfigureAwait(false);
    }
}
