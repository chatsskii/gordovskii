﻿using Gordovskii.Finam.TransaqConnectorWrapper.Model;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml.Linq;

namespace Gordovskii.Finam.TransaqConnectorWrapper
{
    public static class TransaqConnector
    {
        private static Action<string> _callback;

        private static readonly CallbackDelegate _callbackDelegate = new CallbackDelegate(HandleCallback);
        private static readonly GCHandle _callbackHandle = GCHandle.Alloc(_callbackDelegate);

        public static void Initialize(DirectoryInfo loggingDirectory, LoggingLevel loggingLevel = LoggingLevel.Standart)
        {
            if (loggingDirectory == null)
                throw new ArgumentNullException(nameof(loggingDirectory));

            if (!loggingDirectory.Exists)
                throw new DirectoryNotFoundException(nameof(loggingDirectory));

            using (var marshaledPath = UnmanagedString.FromString(loggingDirectory.FullName))
            {
                var resultPointer = Initialize(marshaledPath.Pointer, (int)loggingLevel);

                if (resultPointer == IntPtr.Zero)
                    return;

                using (var marshaledResult = UnmanagedString.FromPointer(resultPointer))
                    throw new InvalidOperationException(marshaledResult.Value);
            }
        }
        public static void Uninitialize()
        {
            var resultPointer = UnInitialize();

            if (resultPointer == IntPtr.Zero)
                return;

            using (var result = UnmanagedString.FromPointer(resultPointer))
                throw new InvalidOperationException(result.Value);
        }
        public static Result Connect(ConnectMessage connectMessage)
        {
            if (connectMessage == null)
                throw new ArgumentNullException(nameof(connectMessage));

            if (string.IsNullOrEmpty(connectMessage.Login))
                throw new ArgumentNullException(nameof(connectMessage.Login));

            if (string.IsNullOrEmpty(connectMessage.Password))
                throw new ArgumentNullException(nameof(connectMessage.Password));

            if (string.IsNullOrEmpty(connectMessage.Host))
                throw new ArgumentNullException(nameof(connectMessage.Host));

            if (connectMessage.Port <= 0)
                throw new ArgumentException(nameof(connectMessage.Port));

            if (connectMessage.Language == ConnectLanguage.Undefiened)
                throw new ArgumentException(nameof(connectMessage.Language));

            if (connectMessage.RequestDelay < ConnectMessage.MinimumRequestDelay)
                throw new ArgumentNullException(nameof(connectMessage.RequestDelay));

            if (connectMessage.PushUnionsLimits < TimeSpan.Zero)
                throw new ArgumentException(nameof(connectMessage.PushUnionsLimits));

            if (connectMessage.PushPositionsEquity < TimeSpan.Zero)
                throw new ArgumentException(nameof(connectMessage.PushPositionsEquity));

            string command = connectMessage.ToCommand().ToString();
            var responseMessage = XElement.Parse(SendCommand(command));
            var result = responseMessage.ToResult();

            return result;
        }
        public static NewOrderResult NewOrder(NewOrderMessage newOrderMessage)
        {
            if (newOrderMessage == null)
                throw new ArgumentNullException(nameof(newOrderMessage));

            if (newOrderMessage.Security == null)
                throw new ArgumentNullException(nameof(newOrderMessage.Security));

            string command = newOrderMessage.ToCommand().ToString();
            var responseMessage = XElement.Parse(SendCommand(command));
            var result = responseMessage.ToNewOrderResult();
            return result;
        }
        public static Result ChangePassword(ChangePasswordMessage changePasswordMessage)
        {
            if (changePasswordMessage == null)
                throw new ArgumentNullException(nameof(changePasswordMessage));

            string command = changePasswordMessage.ToCommand().ToString();
            var responseMessage = XElement.Parse(SendCommand(command));
            var result = responseMessage.ToResult();
            return result;
        }
        public static Result Disconnect()
        {
            string command = TransaqConnectorHelper.CreateCommand("disconnect").ToString();
            var responseMessage = XElement.Parse(SendCommand(command));
            var result = responseMessage.ToResult();

            return result;
        }
        public static bool SetCallback(Action<string> callback)
        {
            if (callback == null)
                throw new ArgumentNullException(nameof(callback));

            if (!SetCallback(_callbackDelegate))
                return false;

            _callback = callback;
            return true;
        }
        public static Result ServerStatus()
        {
            string command = TransaqConnectorHelper.CreateCommand("server_status").ToString();
            var responseMessage = XElement.Parse(SendCommand(command));
            var result = responseMessage.ToResult();

            return result;
        }
        public static Result GetSecurities()
        {
            string command = TransaqConnectorHelper.CreateCommand("server_status").ToString();
            var responseMessage = XElement.Parse(SendCommand(command));
            var result = responseMessage.ToResult();

            return result;
        }
        public static Result Subscribe(SubscribeRequest subscribeRequest)
        {
            string command = subscribeRequest.ToCommand().ToString();
            var responseMessage = XElement.Parse(SendCommand(command));
            var result = responseMessage.ToResult();

            return result;
        }
        public static Result Unsubscribe(UnsubscribeRequest unsubscribeRequest)
        {
            string command = unsubscribeRequest.ToCommand().ToString();
            var responseMessage = XElement.Parse(SendCommand(command));
            var result = responseMessage.ToResult();

            return result;
        }

        private static bool HandleCallback(IntPtr pointer)
        {
            try
            {
                using (var args = UnmanagedString.FromPointer(pointer))
                    _callback.Invoke(args.Value);

                return true;
            }
            catch
            {
                return false;
            }
        }
        private static string SendCommand(string command)
        {
            using (var unmanagedCommand = UnmanagedString.FromString(command))
            {
                var resultPointer = SendCommand(unmanagedCommand.Pointer);
                using (var result = UnmanagedString.FromPointer(resultPointer))
                    return result.Value;
            }
        }

        private delegate bool CallbackDelegate(IntPtr dataPointer);

        #region DllImport
        [DllImport("txmlConnector.dll")]
        private static extern IntPtr Initialize(IntPtr loggingDirectoryPathPointer, int logLevel);

        [DllImport("txmlConnector.dll")]
        private static extern IntPtr UnInitialize();
        [DllImport("txmlconnector.dll")]
        private static extern IntPtr SendCommand(IntPtr dataPointer);
        [DllImport("txmlconnector.dll")]
        private static extern bool SetCallback(CallbackDelegate callback);
        #endregion
    }
}
