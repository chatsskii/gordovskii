﻿using System;

namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public sealed class ConnectMessage
    {
        public const string CommandId = "connect";

        public const string FinamHost = "tr1.finam.ru";
        public const string ReserveFinamHost = "tr2.finam.ru";
        public const string FinamBankHost = "tr1.finambank.ru";
        public const string ReserveFinamBankHost = "tr2.finambank.ru";
        public const string DemoHost = "tr1-demo5.finam.ru";

        public const int FinamPort = 3900;
        public const int FinamBankPort = 3324;
        public const int DemoPort = 3939;

        public static TimeSpan MinimumRequestDelay { get; } = TimeSpan.FromMilliseconds(10);

        public string Login { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public ConnectLanguage Language { get; set; } = ConnectLanguage.Ru;
        public bool AutoPosition { get; set; }
        public bool MicexRegisters { get; set; }
        public bool IncludeMilliseconds { get; set; }
        public bool UseUtcTime { get; set; }
        public ProxyData Proxy { get; set; }
        public TimeSpan RequestDelay { get; set; } = MinimumRequestDelay;
        public TimeSpan SessionTimeout { get; set; } = TimeSpan.FromSeconds(120);
        public TimeSpan RequestTimeout { get; set; } = TimeSpan.FromSeconds(20);
        public TimeSpan PushUnionsLimits { get; set; } = TimeSpan.Zero;
        public TimeSpan PushPositionsEquity { get; set; } = TimeSpan.Zero;
    }
}
