﻿namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public sealed class ProxyData
    {
        public string Type { get; set; }
        public string Address { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }

}
