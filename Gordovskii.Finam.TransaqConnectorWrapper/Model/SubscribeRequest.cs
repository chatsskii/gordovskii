﻿using System.Collections.Generic;
using System.Linq;

namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public sealed class SubscribeRequest
    {
        public const string CommandId = "subscribe";

        public IEnumerable<SecurityMessage> AllTrades { get; set; } = Enumerable.Empty<SecurityMessage>();
        public IEnumerable<SecurityMessage> Quotations { get; set; } = Enumerable.Empty<SecurityMessage>();
        public IEnumerable<SecurityMessage> Quotes { get; set; } = Enumerable.Empty<SecurityMessage>();
    }
}
