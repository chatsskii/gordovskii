﻿namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public sealed class SecurityMessage
    {
        public string Board { get; set; }
        public string SecurityCode { get; set; }
    }
}
