﻿using System;

namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public sealed class NewOrderMessage
    {
        public const string CommandId = "neworder";

        public SecurityMessage Security { get; set; }
        public string Client { get; set; }
        public string Union { get; set; }
        public double? Price { get; set; }
        public int? Quantity { get; set; }
        public int? HiddenQuantity { get; set; }
        public TradeDirection BuySell { get; set; }
        public UnfilledType Unfilled { get; set; }
        public bool MarginAvailable { get; set; }
        public bool WithoutSplit { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
