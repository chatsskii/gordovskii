﻿namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public enum TradeDirection
    { 
        Undefined = 0,
        Buy,
        Sell,
    }

}
