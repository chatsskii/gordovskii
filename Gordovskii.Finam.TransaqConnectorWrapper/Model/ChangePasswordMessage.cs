﻿namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public sealed class ChangePasswordMessage
    {
        public const string CommandId = "change_pass";

        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

}
