﻿namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public enum UnfilledType
    {
        Undefined = 0,
        PutInQueue,
        FOK,
        IOC,
    }
}
