﻿namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public sealed class Result
    {
        public bool Success { get; set; }
    }
}
