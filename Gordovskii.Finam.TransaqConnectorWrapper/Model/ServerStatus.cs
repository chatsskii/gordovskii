﻿namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public sealed class ServerStatus
    {
        public bool Connected { get; set; }
    }
}
