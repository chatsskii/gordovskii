﻿namespace Gordovskii.Finam.TransaqConnectorWrapper.Model
{
    public sealed class NewOrderResult
    {
        public bool Success { get; set; }
        public string TransactionId { get; set; }
    }

}
