﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Gordovskii.Finam.TransaqConnectorWrapper
{
    internal sealed class UnmanagedString : IDisposable
    {
        private readonly bool _needFreeMemory = false;
        public string Value { get; }
        public IntPtr Pointer { get; }

        public UnmanagedString(string value)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
            Pointer = WriteString(value);
            _needFreeMemory = true;
        }

        private UnmanagedString(IntPtr pointer)
        {
            Pointer = pointer;
            Value = ReadString(pointer);
            _needFreeMemory = false;
        }

        private IntPtr WriteString(string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value + "\0");
            int size = Marshal.SizeOf(bytes[0]) * bytes.Length;
            var pointer = Marshal.AllocHGlobal(size);
            Marshal.Copy(bytes, 0, pointer, bytes.Length);
            return pointer;
        }
        private string ReadString(IntPtr pointer)
        {
            int length = Marshal.PtrToStringAnsi(pointer).Length;
            var bytes = new byte[length];
            Marshal.Copy(pointer, bytes, 0, length);
            FreeMemory(pointer);
            return Encoding.UTF8.GetString(bytes);
        }

        public static UnmanagedString FromString(string value) => new UnmanagedString(value);
        public static UnmanagedString FromPointer(IntPtr pointer) => new UnmanagedString(pointer);

        public void Dispose()
        {
            if (_needFreeMemory)
                Marshal.FreeHGlobal(Pointer);
        }

        [DllImport("txmlconnector.dll")]
        private static extern bool FreeMemory(IntPtr pData);
    }
}
