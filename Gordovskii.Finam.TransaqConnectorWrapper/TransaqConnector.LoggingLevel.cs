﻿namespace Gordovskii.Finam.TransaqConnectorWrapper
{
    public enum LoggingLevel
    {
        Undefiened = 0,
        Minimum = 1,
        Standart = 2,
        Maximum = 3,
    }
}
