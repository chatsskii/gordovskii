﻿using Gordovskii.Finam.TransaqConnectorWrapper.Model;
using System.Xml.Linq;

namespace Gordovskii.Finam.TransaqConnectorWrapper
{
    internal static class TransaqConnectorHelper
    {
        public static XElement CreateCommand(string id)
        {
            var command = new XElement("command");
            command.Add(new XAttribute("id", id));
            return command;
        }
        public static XElement ToCommand(this ConnectMessage connectRequest)
        {
            var proxy = (connectRequest.Proxy != null) ? new XElement("proxy") : null;

            if (connectRequest.Proxy != null)
            {
                proxy.Add(new XAttribute("type", connectRequest.Proxy.Type));
                proxy.Add(new XAttribute("addr", connectRequest.Proxy.Address));
                proxy.Add(new XAttribute("login", connectRequest.Proxy.Login));
                proxy.Add(new XAttribute("password", connectRequest.Proxy.Password));
            }

            var command = CreateCommand(ConnectMessage.CommandId);

            command.Add(new XElement("login", connectRequest.Login));
            command.Add(new XElement("password", connectRequest.Password));
            command.Add(new XElement("host", connectRequest.Host));
            command.Add(new XElement("port", connectRequest.Port.ToString()));
            //command.Add(new XElement("utc_time", connectRequest.UseUtcTime.ToString().ToLower()));
            if (proxy != null)
                command.Add(new XElement(proxy));
            command.Add(new XElement("rqdelay", (int)connectRequest.RequestDelay.TotalMilliseconds));
            command.Add(new XElement("session_timeout", (int)connectRequest.SessionTimeout.TotalSeconds));
            command.Add(new XElement("request_timeout", (int)connectRequest.RequestTimeout.TotalSeconds));
            command.Add(new XElement("push_u_limits", (int)connectRequest.PushUnionsLimits.TotalSeconds));
            command.Add(new XElement("pushpos_equity", (int)connectRequest.PushPositionsEquity.TotalSeconds));

            return command;
        }
        public static XElement ToCommand(this SubscribeRequest subscribeRequest)
        {
            var command = CreateCommand(SubscribeRequest.CommandId);

            var alltrades = new XElement("alltrades");
            foreach (var alltrade in subscribeRequest.AllTrades)
                alltrades.Add(alltrade.ToCommand());

            var quotations = new XElement("quotations");
            foreach (var quotation in subscribeRequest.Quotations)
                quotations.Add(quotation.ToCommand());

            var quotes = new XElement("quotes");
            foreach (var quote in subscribeRequest.Quotes)
                quotes.Add(quote.ToCommand());

            command.Add(alltrades);
            command.Add(quotations);
            command.Add(quotes);
            return command;
        }
        public static XElement ToCommand(this UnsubscribeRequest unsubscribeRequest)
        {
            var command = CreateCommand(UnsubscribeRequest.CommandId);

            var alltrades = new XElement("alltrades");
            foreach (var alltrade in unsubscribeRequest.AllTrades)
                alltrades.Add(alltrade.ToCommand());

            var quotations = new XElement("quotations");
            foreach (var quotation in unsubscribeRequest.Quotations)
                quotations.Add(quotation.ToCommand());

            var quotes = new XElement("quotes");
            foreach (var quote in unsubscribeRequest.Quotes)
                quotes.Add(quote.ToCommand());

            command.Add(alltrades);
            command.Add(quotations);
            command.Add(quotes);
            return command;
        }

        public static XElement ToCommand(this SecurityMessage securityData)
        {
            var security = new XElement("security");
            security.Add(new XElement("board", securityData.Board));
            security.Add(new XElement("seccode", securityData.SecurityCode));
            return security;
        }

        public static XElement ToCommand(this NewOrderMessage newOrderMessage)
        {
            var command = CreateCommand(NewOrderMessage.CommandId);
            command.Add(newOrderMessage.Security.ToCommand());
            command.Add(new XElement("client", newOrderMessage.Client));
            command.Add(new XElement("union", newOrderMessage.Union));

            if (newOrderMessage.Price.HasValue)
                command.Add(new XElement("price", newOrderMessage.Price.Value));
            else
                command.Add(new XElement("bymarket"));

            if (newOrderMessage.HiddenQuantity.HasValue)
                command.Add(new XElement("hidden", newOrderMessage.HiddenQuantity.Value));

            if (newOrderMessage.Quantity.HasValue)
                command.Add(new XElement("quantity", newOrderMessage.Quantity.Value));

            command.Add(new XElement("buysell", newOrderMessage.BuySell == TradeDirection.Buy ? "B" : "S"));

            switch (newOrderMessage.Unfilled)
            {
                case UnfilledType.PutInQueue:
                command.Add(new XElement("unfilled", "PutInQueue"));
                break;

                case UnfilledType.FOK:
                command.Add(new XElement("unfilled", "FOK"));
                break;

                case UnfilledType.IOC:
                command.Add(new XElement("unfilled", "IOC"));
                break;
            }

            if (newOrderMessage.MarginAvailable)
                command.Add(new XElement("usecredit"));

            if (newOrderMessage.WithoutSplit)
                command.Add(new XElement("nosplit"));

            if (newOrderMessage.ExpirationDate.HasValue)
                command.Add(new XElement("expdate", newOrderMessage.ExpirationDate.Value.ToString("dd.MM.yyyy HH.mm.ss")));

            return command;
        }
        public static XElement ToCommand(this ChangePasswordMessage changePasswordMessage)
        {
            var command = CreateCommand(ChangePasswordMessage.CommandId);
            command.Add(new XAttribute("oldpass", changePasswordMessage.OldPassword));
            command.Add(new XAttribute("newpass", changePasswordMessage.NewPassword));

            return command;
        }
        public static ServerStatus ToServerStatus(this XElement message) => new ServerStatus()
        {
            Connected = bool.Parse(message.Element("server_status").Attribute("connected").Value),
        };

        public static Result ToResult(this XElement message) => new Result()
        {
            Success = bool.Parse(message.Attribute("success").Value),
        };

        public static NewOrderResult ToNewOrderResult(this XElement message) => new NewOrderResult()
        {
            Success = message.Attribute("success").Value == "true",
            TransactionId = message.Attribute("transactionid").Value,
        };
    }
}
