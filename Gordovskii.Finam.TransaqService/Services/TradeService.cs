﻿using Google.Protobuf.WellKnownTypes;
using Gordovskii.Finam.TransaqService.Grpc;
using Grpc.Core;

namespace Gordovskii.Finam.TransaqService.Services
{
    internal sealed class TradeService : Grpc.TradeService.TradeServiceBase
    {
        private readonly QuotationRecordCollection _quotationRecordCollection;
        private readonly ILogger<TradeService> _logger;

        public TradeService(QuotationRecordCollection quotationRecordCollection, ILogger<TradeService> logger)
        {
            _quotationRecordCollection = quotationRecordCollection ?? throw new ArgumentNullException(nameof(quotationRecordCollection));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public override async Task TradeStream(TradeStreamRequest request, IServerStreamWriter<TradeStreamResponse> responseStream, ServerCallContext context)
        {
            var quotationRecord = _quotationRecordCollection.EnsureQuotationRecord(request.SecurityId);
            var semaphore = new SemaphoreSlim(0);

            using (var subscription = quotationRecord.SubscribeToUpdate(() => semaphore.Release()))
            {
                while (!context.CancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        await semaphore.WaitAsync(context.CancellationToken);
                        await responseStream.WriteAsync(new TradeStreamResponse()
                        {
                            SecurityId = quotationRecord.SecurityId,
                            Price = (float)quotationRecord.LastTradePrice,
                            Quantity = quotationRecord.LastTradeQuantity,
                            Time = quotationRecord.LastTradeTime.ToTimestamp(),
                        }, context.CancellationToken);
                    }
                    catch (OperationCanceledException)
                    {
                        _logger.LogInformation("Trade stream canceled by client.");
                    }
                }
            }
        }
    }
}
