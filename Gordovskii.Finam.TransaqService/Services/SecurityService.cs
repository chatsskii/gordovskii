﻿using Gordovskii.Finam.TransaqService.Data;
using Gordovskii.Finam.TransaqService.Grpc;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;

namespace Gordovskii.Finam.TransaqService.Services
{
    internal class SecurityService : Grpc.SecurityService.SecurityServiceBase
    {
        private readonly ILogger<SecurityService> _logger;
        private readonly IDbContextFactory<TransaqDbContext> _dbContextFactory;

        public SecurityService(ILogger<SecurityService> logger, IDbContextFactory<TransaqDbContext> dbContextFactory)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
        }
        public override async Task<ListSecuritiesResponse> List(ListSecuritiesRequest request, ServerCallContext context)
        {
            var response = new ListSecuritiesResponse();

            await using (var dbContext = await _dbContextFactory.CreateDbContextAsync())
            {
                var query = dbContext.Securities.AsQueryable();

                if (request.SecurityCodeFilter != null)
                    query = query.Where(x => x.Code == request.SecurityCodeFilter);
                if (request.BoardFilter != null)
                    query = query.Where(x => x.BoardId == request.BoardFilter);

                response.Total = request.Total ? await query.CountAsync(context.CancellationToken) : 0;
                query = query.OrderBy(x => x.Code).Skip(request.Skip).Take(request.Take);

                response.Securities.AddRange(query.ToGrpc());
            }

            return response;
        }

    }
}
