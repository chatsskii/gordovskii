﻿using SecurityData = Gordovskii.Finam.TransaqService.Data.Security;
using SecurityGrpc = Gordovskii.Finam.TransaqService.Grpc.Security;

namespace Gordovskii.Finam.TransaqService.Services
{
    internal static class SecurityExtensions
    {
        public static SecurityGrpc ToGrpc(this SecurityData data) => new SecurityGrpc()
        {
            Id = data.Id,
            Code = data.Code,
            Board = data.BoardId,
            Active = data.Active,
            Name = data.ShortName,
            Price = data.PointCost * data.MinimumStep * Math.Pow(-10, data.Decimals),
        };

        public static IEnumerable<SecurityGrpc> ToGrpc(this IEnumerable<SecurityData> data)
            => data.Select(ToGrpc);
    }
}
