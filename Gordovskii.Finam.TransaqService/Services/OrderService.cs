﻿using Google.Protobuf.WellKnownTypes;
using Gordovskii.Finam.TransaqConnectorWrapper;
using Gordovskii.Finam.TransaqConnectorWrapper.Model;
using Gordovskii.Finam.TransaqService.Data;
using Gordovskii.Finam.TransaqService.Grpc;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;

namespace Gordovskii.Finam.TransaqService.Services
{
    internal class OrderService : Grpc.OrderService.OrderServiceBase
    {
        private readonly ILogger<ConnectionService> _logger;
        private readonly IDbContextFactory<TransaqDbContext> _dbContextFactory;

        public OrderService(ILogger<ConnectionService> logger, IDbContextFactory<TransaqDbContext> dbContextFactory)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
        }

        public override async Task<CreateOrderResponse> Create(CreateOrderRequest request, ServerCallContext context)
        {
            await using (var dbContext = await _dbContextFactory.CreateDbContextAsync(context.CancellationToken))
            {
                var security = await dbContext.Securities.FindAsync(new object[] { request.SecurityId }, context.CancellationToken)
                    ?? throw new RpcException(new Status(StatusCode.InvalidArgument, $"{nameof(request.SecurityId)} [{request.SecurityId}] not found."));

                var board = await dbContext.Boards.FindAsync(new object[] { security.BoardId }, context.CancellationToken);
                var client = await dbContext.Clients.SingleAsync(x => x.MarketId == board!.MarketId, context.CancellationToken);

                var result = TransaqConnector.NewOrder(new NewOrderMessage()
                {
                    Security = new SecurityMessage() 
                    { 
                        SecurityCode = security.Code, 
                        Board = security.BoardId, 
                    },
                    Client = client.Id,
                    Union = client.Union,
                    Price = request.Price,
                    Unfilled = UnfilledType.PutInQueue,
                    BuySell = request.Quantity > 0 ? TradeDirection.Buy : TradeDirection.Sell,
                    Quantity = Math.Abs(request.Quantity),
                    MarginAvailable = request.MarginAvailable,
                });

                if (!result.Success)
                    throw new RpcException(new Status(StatusCode.Internal, "Failed."));

                return new CreateOrderResponse()
                {
                    OrderId = result.TransactionId,
                };
            }
        }
        public override async Task<ListOrdersResponse> List(ListOrdersRequest request, ServerCallContext context)
        {
            await using (var dbContext = await _dbContextFactory.CreateDbContextAsync(context.CancellationToken))
            {
                var orders = dbContext.Orders;
                var response = new ListOrdersResponse();

                if (request.Total)
                    response.Total = orders.Count();

                foreach (var order in orders)
                    response.Orders.Add(new Grpc.Order() 
                    { 
                        Id = order.Id,
                        SecurityId = order.SecurityId,
                        ClientId = order.ClientId,
                        Price = (float)order.Price,
                        Quantity = order.Quantity,
                        Status = (Grpc.OrderStatus)(int)order.Status,
                        RegisteredAt = DateTime.SpecifyKind(order.RegisteredAt, DateTimeKind.Utc).ToTimestamp(),
                    });

                return response;
            }
        }
    }
}
