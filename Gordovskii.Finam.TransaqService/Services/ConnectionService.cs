using Gordovskii.Finam.TransaqConnectorWrapper;
using Gordovskii.Finam.TransaqConnectorWrapper.Model;
using Gordovskii.Finam.TransaqService.Data;
using Gordovskii.Finam.TransaqService.Grpc;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;
using System.Threading.Channels;

namespace Gordovskii.Finam.TransaqService.Services
{
    internal class ConnectionService : Grpc.ConnectionService.ConnectionServiceBase, IDisposable
    {
        private readonly ILogger<ConnectionService> _logger;
        private readonly IDbContextFactory<TransaqDbContext> _dbContextFactory;
        private readonly ConnectionMonitor _connectionMonitor;

        private readonly Channel<bool> _connectionChannel = Channel.CreateUnbounded<bool>(new UnboundedChannelOptions()
        {
            SingleWriter = true,
            SingleReader = false,
        });

        public ConnectionService(ILogger<ConnectionService> logger, IDbContextFactory<TransaqDbContext> dbContextFactory, ConnectionMonitor connectionMonitor)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
            _connectionMonitor = connectionMonitor ?? throw new ArgumentNullException(nameof(connectionMonitor));

            _connectionMonitor.ConnectedChanged += _connectionMonitor_ConnectedChanged;
        }

        public override async Task<ConnectResponse> Connect(ConnectRequest request, ServerCallContext context)
        {
            await using (var dbContext = _dbContextFactory.CreateDbContext())
                await dbContext.Database.EnsureCreatedAsync();

            var result = TransaqConnector.Connect(new ConnectMessage()
            {
                Login = request.Login,
                Password = request.Password,
                Host = request.Host,
                Port = request.Port,
                UseUtcTime = true,
                Language = ConnectLanguage.En,
            });

            if (!result.Success)
            {
                _logger.LogInformation($"{DateTime.Now}: Connection failed.");
                throw new RpcException(new Status(StatusCode.Internal, "Failed to connect."));
            }

            _logger.LogInformation($"{DateTime.Now}: Connection succeed.");
            return new ConnectResponse();
        }
        public override async Task<DisconnectResponse> Disconnect(DisconnectRequest request, ServerCallContext context)
        {
            var result = TransaqConnector.Disconnect();

            if (!result.Success)
            {
                _logger.LogInformation($"{DateTime.Now}: Disconnection failed.");
                throw new RpcException(new Status(StatusCode.Internal, "Failed to disconnect."));
            }

            await using (var dbContext = _dbContextFactory.CreateDbContext())
                await dbContext.Database.EnsureDeletedAsync();

            _logger.LogInformation($"{DateTime.Now}: Disconnection succeeds.");
            return new DisconnectResponse();
        }
        public override Task<GetConnectionResponse> GetConnection(GetConnectionRequest request, ServerCallContext context)
            => Task.FromResult(new GetConnectionResponse() { Connected = _connectionMonitor.Connected });

        public override async Task ConnectionStream(ConnectionRequest request, IServerStreamWriter<ConnectionResponse> responseStream, ServerCallContext context)
        {
            await responseStream.WriteAsync(new ConnectionResponse() { Connected = _connectionMonitor.Connected }, context.CancellationToken).ConfigureAwait(false);

            while (await _connectionChannel.Reader.WaitToReadAsync(context.CancellationToken).ConfigureAwait(false))
            {
                if (_connectionChannel.Reader.TryRead(out bool connected))
                    await responseStream.WriteAsync(new ConnectionResponse() { Connected = connected }, context.CancellationToken).ConfigureAwait(false);
            }
        }

        public void Dispose()
        {
            _connectionChannel.Writer.Complete();
            _connectionMonitor.ConnectedChanged -= _connectionMonitor_ConnectedChanged;
        }

        private void _connectionMonitor_ConnectedChanged(object? sender, bool e) => _connectionChannel.Writer.TryWrite(e);
    }
}