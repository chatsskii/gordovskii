﻿using Gordovskii.Finam.TransaqService.Grpc;
using Grpc.Core;

namespace Gordovskii.Finam.TransaqService.Services
{
    internal sealed class PriceService : Grpc.PriceService.PriceServiceBase
    {
        private readonly QuotationRecordCollection _quotationRecordCollection;
        private readonly ILogger<PriceService> _logger;

        public PriceService(QuotationRecordCollection quotationRecordCollection, ILogger<PriceService> logger)
        {
            _quotationRecordCollection = quotationRecordCollection ?? throw new ArgumentNullException(nameof(quotationRecordCollection));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public override async Task PriceStream(PriceStreamRequest request, IServerStreamWriter<PriceStreamResponse> responseStream, ServerCallContext context)
        {
            var quotationRecord = _quotationRecordCollection.EnsureQuotationRecord(request.SecurityId);
            var semaphore = new SemaphoreSlim(0);

            double price = quotationRecord.LastTradePrice;

            using (var subscription = quotationRecord.SubscribeToUpdate(() => 
            {
                if (price != quotationRecord.LastTradePrice)
                {
                    price = quotationRecord.LastTradePrice;
                    semaphore.Release();
                }
            }))
            {
                while (!context.CancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        await semaphore.WaitAsync(context.CancellationToken).ConfigureAwait(false);
                        await responseStream.WriteAsync(new PriceStreamResponse()
                        {
                            SecurityId = quotationRecord.SecurityId,
                            Price = (float)quotationRecord.LastTradePrice,
                        }, context.CancellationToken).ConfigureAwait(false);
                    }
                    catch(OperationCanceledException)
                    {
                        _logger.LogInformation("Price stream canceled by user.");
                    }
                }
            }
        }
    }
}
