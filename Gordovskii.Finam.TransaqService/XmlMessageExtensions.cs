﻿using Gordovskii.Finam.TransaqService.Data;
using System.Xml.Linq;

namespace Gordovskii.Finam.TransaqService
{
    internal static class XmlMessageExtensions
    {
        public static Market ToMarket(this XElement element) => new Market()
        {
            Id = element.Attribute("id")!.Value,
            Name = element.Value,
        };

        public static Board ToBoard(this XElement element) => new Board()
        {
            Id = element.Attribute("id")!.Value,
            MarketId = element.Element("market")!.Value,
            Name = element.Element("name")!.Value,
            Type = (BoardTradeType)(1 + int.Parse(element.Element("type")!.Value)),
        };

        public static CandleKind ToCandleKind(this XElement element) => new CandleKind()
        {
            Id = element.Element("id")!.Value,
            Period = TimeSpan.FromSeconds(int.Parse(element.Element("period")!.Value)),
            Name = element.Element("name")!.Value,
        };

        public static Security ToSecurity(this XElement element) => new Security()
        {
            Id = element.Attribute("secid")!.Value,
            Active = element.Attribute("active")!.Value == "true",
            Code = element.Element("seccode")!.Value,
            BoardId = element.Element("board")!.Value,
            ShortName = element.Element("shortname")!.Value,
            Decimals = int.Parse(element.Element("decimals")!.Value),
            MinimumStep = double.Parse(element.Element("minstep")!.Value.Replace('.', ',')),
            LotSize = int.Parse(element.Element("lotsize")!.Value),
            PointCost = double.Parse(element.Element("point_cost")!.Value.Replace('.', ',')),
            Type = ParseSecurityType(element.Element("sectype")!.Value),
        };

        public static Pit ToPit(this XElement element) => new Pit()
        {
            SecurityCode = element.Attribute("seccode")!.Value,
            BoardId = element.Attribute("board")!.Value,
            Decimals = int.Parse(element.Element("decimals")!.Value),
            MinimumStep = double.Parse(element.Element("minstep")!.Value.Replace('.', ',')),
            LotSize = int.Parse(element.Element("lotsize")!.Value),
            PointCost = double.Parse(element.Element("point_cost")!.Value.Replace('.', ',')),
        };

        public static Client ToClient(this XElement element) => new Client()
        {
            Id = element.Attribute("id")!.Value,
            Removed = element.Attribute("remove")!.Value == "true",
            ClientType = ParseClientType(element.Element("type")!.Value),
            MarketId = element.Element("market")!.Value,
            Union = element.Element("union")?.Value ?? string.Empty,
        };

        public static Quotation ToQuotation(this XElement element)
        {
            var quotation = new Quotation()
            {
                SecurityId = element.Attribute("secid")!.Value,
            };

            var lastTradePrice = element.Element("last")?.Value.Replace('.', ',');
            if (!string.IsNullOrEmpty(lastTradePrice))
                quotation.LastTradePrice = double.Parse(lastTradePrice);

            var lastTradeQuantity = element.Element("quantity")?.Value;
            if (!string.IsNullOrEmpty(lastTradeQuantity))
                quotation.LastTradeQuantity = int.Parse(lastTradeQuantity);

            var lastTradeTime = element.Element("time")?.Value;
            if (!string.IsNullOrEmpty(lastTradeTime))
                quotation.LastTradeTime = DateTime.SpecifyKind(DateTime.Parse(lastTradeTime), DateTimeKind.Utc);

            //AccuredInterest = double.Parse(element.Element("accruedintvalue")!.Value),
            //OpenPrice = double.Parse(element.Element("open")!.Value),
            //BestBid = double.Parse(element.Element("bid")!.Value),
            //BestOffer = double.Parse(element.Element("offer")!.Value),
            //TradesCount = int.Parse(element.Element("numtrades")!.Value),
            //TradesVolume = int.Parse(element.Element("voltoday")!.Value),
            ////OpenInterest = int.Parse(element.Element("openpositions")!.Value),
            ////OpenInterestDelta = int.Parse(element.Element("deltapositions")!.Value),
            //LastTradePrice = double.Parse(element.Element("last")!.Value),
            //LastTradeQuantity = int.Parse(element.Element("quantity")!.Value),
            //LastTradeTime = DateTime.Parse(element.Element("time")!.Value),
            //LastTradePriceDelta = double.Parse(element.Element("change")!.Value),
            //HighPrice = double.Parse(element.Element("high")!.Value),
            //LowPrice = double.Parse(element.Element("low")!.Value),
            //ClosePrice = double.Parse(element.Element("closeprice")!.Value),


            return quotation;
        }

        public static Order ToOrder(this XElement element) => new Order()
        {
            Id = element.Attribute("transactionid")!.Value,
            SecurityId = element.Element("secid")!.Value,
            ClientId = element.Element("client")!.Value,
            Status = ParseOrderStatus(element.Element("status")!.Value),
            RegisteredAt = DateTime.SpecifyKind(DateTime.Parse(element.Element("time")!.Value), DateTimeKind.Utc),
            Price = double.Parse(element.Element("price")!.Value.Replace('.', ',')),
            Quantity = int.Parse(element.Element("quantity")!.Value) * ((element.Element("buysell")!.Value == "B") ? 1 : -1),
        };

        public static SecurityType ParseSecurityType(string value)
        {
            switch (value)
            {
                case ("SHARE"):
                return SecurityType.Share;

                case ("BOND"):
                return SecurityType.Bond;

                case ("FUT"):
                return SecurityType.Fut;

                case ("OPT"):
                return SecurityType.Opt;

                case ("GKO"):
                return SecurityType.Gko;

                case ("FOB"):
                return SecurityType.Fob;

                case ("IDX"):
                return SecurityType.Idx;

                case ("QUOTES"):
                return SecurityType.Quotes;

                case ("CURRENCY"):
                return SecurityType.Currency;

                case ("ADR"):
                return SecurityType.Adr;

                case ("NYSE"):
                return SecurityType.Nyse;

                case ("METAL"):
                return SecurityType.Metal;

                case ("OIL"):
                return SecurityType.Oil;

                default:
                return SecurityType.Undefined;

            }
        }
        public static ClientType ParseClientType(string value)
        {
            switch (value)
            {
                case ("spot"):
                return ClientType.Spot;

                case ("leverage"):
                return ClientType.Leverage;

                case ("margin_level"):
                return ClientType.MarginLevel;

                case ("mct"):
                return ClientType.Mct;

                default:
                return ClientType.Undefined;
            }
        }
        public static OrderStatus ParseOrderStatus(string value)
        {
            switch (value)
            {
                case ("cancelled"):
                return OrderStatus.Cancelled;

                case ("active"):
                return OrderStatus.Active;

                case ("denied"):
                return OrderStatus.Denied;

                case ("disabled"):
                return OrderStatus.Disabled;

                case ("expired"):
                return OrderStatus.Expired;

                case ("failed"):
                return OrderStatus.Failed;

                case ("matched"):
                return OrderStatus.Matched;

                case ("rejected"):
                return OrderStatus.Rejected;

                default:
                return OrderStatus.Undefined;
            }
        }
    }
}
