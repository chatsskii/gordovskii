﻿using Gordovskii.Finam.TransaqService.Data;
using Microsoft.EntityFrameworkCore;

namespace Gordovskii.Finam.TransaqService
{
    internal sealed class QuotationRecordFactory
    {
        private readonly CallbackHandler _callbackHandler;
        private readonly IDbContextFactory<TransaqDbContext> _dbContextFactory;

        public QuotationRecordFactory(CallbackHandler callbackHandler, IDbContextFactory<TransaqDbContext> dbContextFactory)
        {
            _callbackHandler = callbackHandler ?? throw new ArgumentNullException(nameof(callbackHandler));
            _dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
        }

        public QuotationRecord Create(string securityId)
        {
            using (var dbContext = _dbContextFactory.CreateDbContext())
            {
                var security = dbContext.Securities.Find(securityId) ?? throw new Exception();
                return new QuotationRecord(security.Id, security.Code, security.BoardId, _callbackHandler);
            }
        }
    }
}
