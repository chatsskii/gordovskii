﻿using Gordovskii.Finam.TransaqConnectorWrapper;
using Gordovskii.Finam.TransaqConnectorWrapper.Model;
using Gordovskii.Finam.TransaqService.Data;

namespace Gordovskii.Finam.TransaqService
{
    internal sealed class QuotationRecord : IDisposable
    {
        private readonly CallbackHandler _callbackHandler;

        private readonly HashSet<Action> _updateActions = new HashSet<Action>();
        private readonly object _updateActionsLock = new object();

        public string SecurityId { get; }
        public string SecurityCode { get; }
        public string SecurityBoard { get; }

        public double LastTradePrice { get; private set; }
        public int LastTradeQuantity { get; private set; }
        public DateTime LastTradeTime { get; private set; }

        public QuotationRecord(string securityId, string securityCode, string securityBoard, CallbackHandler callbackHandler)
        {
            SecurityId = securityId ?? throw new ArgumentNullException(nameof(securityId));
            SecurityCode = securityCode ?? throw new ArgumentNullException(nameof(securityCode));
            SecurityBoard = securityBoard ?? throw new ArgumentNullException(nameof(securityBoard));
            _callbackHandler = callbackHandler ?? throw new ArgumentNullException(nameof(callbackHandler));

            _callbackHandler.QuotationReceived += _callbackHandler_QuotationReceived;
        }

        public IDisposable SubscribeToUpdate(Action action) => QuotationSubscription.Subscribe(this, action);

        public void Dispose()
            => _callbackHandler.QuotationReceived -= _callbackHandler_QuotationReceived;

        private void AddAction(Action action)
        {
            lock (_updateActionsLock)
            {
                if (_updateActions.Count == 0)
                {
                    var securityMessage = new SecurityMessage()
                    {
                        SecurityCode = SecurityCode,
                        Board = SecurityBoard,
                    };

                    TransaqConnector.Subscribe(new SubscribeRequest()
                    {
                        Quotations = new SecurityMessage[] { securityMessage },
                    });
                }

                _updateActions.Add(action);
            }
        }
        private void RemoveAction(Action action)
        {
            lock (_updateActionsLock)
            {
                if (!_updateActions.Remove(action))
                    throw new ArgumentOutOfRangeException(nameof(action));

                if (_updateActions.Count == 0)
                {
                    var securityMessage = new SecurityMessage()
                    {
                        SecurityCode = SecurityCode,
                        Board = SecurityBoard,
                    };

                    TransaqConnector.Unsubscribe(new UnsubscribeRequest()
                    {
                        Quotations = new SecurityMessage[] { securityMessage },
                    });
                }
            }
        }
        private void Update(Quotation quotation)
        {
            bool updated = false;

            if (quotation.LastTradePrice.HasValue)
            {
                if (LastTradePrice != quotation.LastTradePrice.Value)
                {
                    LastTradePrice = quotation.LastTradePrice.Value;
                    updated = true;
                }
            }
            if (quotation.LastTradeQuantity.HasValue)
            {
                if (LastTradeQuantity != quotation.LastTradeQuantity)
                {
                    LastTradeQuantity = quotation.LastTradeQuantity.Value;
                    updated = true;
                }
            }
            if (quotation.LastTradeTime.HasValue)
            {
                if (LastTradeTime != quotation.LastTradeTime)
                {
                    LastTradeTime = quotation.LastTradeTime.Value;
                    updated = true;
                }
            }

            if (updated)
                InvokeUpdateActions();
        }
        private void InvokeUpdateActions()
        {
            lock (_updateActionsLock)
            {
                foreach (var action in _updateActions)
                    action.Invoke();
            }
        }

        private void _callbackHandler_QuotationReceived(object? sender, Quotation e)
        {
            if (e.SecurityId == SecurityId)
                Update(e);
        }

        private sealed class QuotationSubscription : IDisposable
        {
            private readonly QuotationRecord _quotation;
            private readonly Action _action;

            private bool _disposed = false;

            private QuotationSubscription(QuotationRecord quotation, Action action)
            {
                _quotation = quotation ?? throw new ArgumentNullException(nameof(quotation));
                _action = action ?? throw new ArgumentNullException(nameof(action));

                _quotation.AddAction(_action);
            }

            public static QuotationSubscription Subscribe(QuotationRecord quotation, Action action)
                => new QuotationSubscription(quotation, action);

            public void Dispose()
            {
                if (_disposed)
                    throw new ObjectDisposedException(nameof(QuotationSubscription));

                _quotation.RemoveAction(_action);
                _disposed = true;
            }
        }
    }
}
