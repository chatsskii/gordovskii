﻿namespace Gordovskii.Finam.TransaqService
{
    internal sealed class QuotationRecordCollection
    {
        private readonly QuotationRecordFactory _factory;

        private readonly Dictionary<string, QuotationRecord> _quotationBySecurityId 
            = new Dictionary<string, QuotationRecord>();

        private readonly object _quotationBySecurityIdLock = new object();

        public QuotationRecordCollection(QuotationRecordFactory factory)
        {
            _factory = factory ?? throw new ArgumentNullException(nameof(factory));
        }

        public QuotationRecord EnsureQuotationRecord(string securityId)
        {
            lock (_quotationBySecurityIdLock)
            {
                if (_quotationBySecurityId.TryGetValue(securityId, out QuotationRecord? quotation))
                    return quotation;

                quotation = _factory.Create(securityId);
                _quotationBySecurityId.Add(securityId, quotation);
                return quotation;
            }
        }
    }
}
