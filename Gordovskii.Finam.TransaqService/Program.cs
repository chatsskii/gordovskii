using Gordovskii.Finam.TransaqConnectorWrapper;
using Gordovskii.Finam.TransaqService.Data;
using Microsoft.EntityFrameworkCore;

namespace Gordovskii.Finam.TransaqService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            // Additional configuration is required to successfully run gRPC on macOS.
            // For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682

            // Add services to the container.
            builder.Services.AddGrpc();
            builder.Services.AddSingleton<IDbContextFactory<TransaqDbContext>, InMemoryDbContextFactory>();
            builder.Services.AddSingleton<CallbackHandler>();
            builder.Services.AddSingleton<ConnectionMonitor>();

            // Quotations
            builder.Services.AddSingleton<QuotationRecordFactory>();
            builder.Services.AddSingleton<QuotationRecordCollection>();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            app.MapGrpcService<Services.ConnectionService>();
            app.MapGrpcService<Services.OrderService>();
            app.MapGrpcService<Services.SecurityService>();
            app.MapGrpcService<Services.TradeService>();
            app.MapGrpcService<Services.PriceService>();
            app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

            TransaqConnector.Initialize(new DirectoryInfo("C:\\log"));
            app.Run();
            TransaqConnector.Uninitialize();
        }
    }
}