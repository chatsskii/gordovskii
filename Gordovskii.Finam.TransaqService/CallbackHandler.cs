﻿using Gordovskii.Finam.TransaqConnectorWrapper;
using Gordovskii.Finam.TransaqService.Data;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;

namespace Gordovskii.Finam.TransaqService
{
    internal sealed class CallbackHandler
    {
        public event EventHandler<bool>? ConnectedReceived;
        public event EventHandler<Quotation>? QuotationReceived;

        private const string MarketsCallback = "markets";
        private const string BoardsCallback = "boards";
        private const string CandleKindsCallback = "candlekinds";
        private const string SecuritiesCallback = "securities";
        private const string ClientCallback = "client";
        private const string ServerStatusCallback = "server_status";
        private const string QuotationsCallback = "quotations";
        private const string OrdersCallback = "orders";

        private readonly IDbContextFactory<TransaqDbContext> _contextFactory;
        private readonly ILogger<CallbackHandler> _logger;

        public CallbackHandler(IDbContextFactory<TransaqDbContext> contextFactory, ILogger<CallbackHandler> logger)
        {
            _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            TransaqConnector.SetCallback(HandleCallback);
        }

        public void HandleCallback(string callback)
        {
            var element = XElement.Parse(callback);

            switch (element.Name.ToString())
            {
                case (MarketsCallback):
                HandleMarkets(element);
                break;

                case (BoardsCallback):
                HandleBoards(element);
                break;

                case (CandleKindsCallback):
                HandleCandleKinds(element);
                break;

                case (SecuritiesCallback):
                HandleSecurities(element);
                break;

                case (ClientCallback):
                HandleClient(element);
                break;

                case (ServerStatusCallback):
                HandleServerStatus(element);
                break;

                case (QuotationsCallback):
                HandleQuotations(element);
                break;

                case (OrdersCallback):
                HandleOrders(element);
                break;
            }
        }

        private void HandleMarkets(XElement marketsElement)
        {
            using (var dbContext = _contextFactory.CreateDbContext())
            {
                foreach (var marketElement in marketsElement.Elements())
                    dbContext.Markets.Add(marketElement.ToMarket());

                dbContext.SaveChanges();
            }
        }
        private void HandleBoards(XElement boardsElement)
        {
            using (var dbContext = _contextFactory.CreateDbContext())
            {
                foreach (var boardElement in boardsElement.Elements())
                {
                    var board = boardElement.ToBoard();

                    if (dbContext.Markets.Find(board.MarketId) == null)
                    {
                        _logger.LogWarning($"Not found market: {board.MarketId}");
                        continue;
                    }

                    dbContext.Boards.Add(board);
                }

                dbContext.SaveChanges();
            }
        }
        private void HandleCandleKinds(XElement candleKindsElement)
        {
            using (var dbContext = _contextFactory.CreateDbContext())
            {
                foreach (var candleKindElement in candleKindsElement.Elements())
                    dbContext.CandleKinds.Add(candleKindElement.ToCandleKind());

                dbContext.SaveChanges();
            }
        }
        private void HandleSecurities(XElement securitiesElement)
        {
            using (var dbContext = _contextFactory.CreateDbContext())
            {
                foreach (var securityElement in securitiesElement.Elements())
                {
                    var security = securityElement.ToSecurity();
                    if (dbContext.Securities.Any(x => x.Id == security.Id))
                        dbContext.Securities.Update(security);
                    else
                        dbContext.Securities.Add(security);
                }

                dbContext.SaveChanges();
            }
        }
        private void HandleClient(XElement clientElement)
        {
            using (var dbContext = _contextFactory.CreateDbContext())
            {
                var client = clientElement.ToClient();
                if (dbContext.Clients.Any(x => x.Id == client.Id))
                    dbContext.Clients.Update(client);
                else
                    dbContext.Clients.Add(client);

                dbContext.SaveChanges();
            }
        }
        private void HandleServerStatus(XElement serverStatusElement)
        {
            var connected = serverStatusElement.Attribute("connected")!.Value == "true";
            OnConnectedReceived(connected);
        }
        private void HandleQuotations(XElement quotationsElement)
        {
            foreach (var quotationElement in quotationsElement.Elements())
                OnQuotationRecieved(quotationElement.ToQuotation());
        }
        private void HandleOrders(XElement ordersElement)
        {
            using (var dbContext = _contextFactory.CreateDbContext())
            {
                foreach (var orderElement in ordersElement.Elements("order"))
                {
                    var order = orderElement.ToOrder();
                    if (order != null)
                    {
                        if (dbContext.Orders.Any(x => x.Id == order.Id))
                            dbContext.Orders.Update(order);
                        else
                            dbContext.Orders.Add(order);
                    }
                }

                dbContext.SaveChanges();
            }
        }

        private void OnConnectedReceived(bool connected) => ConnectedReceived?.Invoke(this, connected);
        private void OnQuotationRecieved(Quotation quotation) => QuotationReceived?.Invoke(this, quotation);
    }
}
