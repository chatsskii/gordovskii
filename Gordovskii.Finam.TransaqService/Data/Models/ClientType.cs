﻿namespace Gordovskii.Finam.TransaqService.Data
{
    public enum ClientType
    { 
        Undefined = 0,
        Spot,
        Leverage,
        MarginLevel,
        Mct,
    }
}
