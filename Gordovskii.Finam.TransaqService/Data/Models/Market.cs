﻿namespace Gordovskii.Finam.TransaqService.Data
{
    public class Market
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ICollection<Board> Boards { get; set; } = new HashSet<Board>();
        public ICollection<Client> Clients { get; set; } = new HashSet<Client>();
    }
}
