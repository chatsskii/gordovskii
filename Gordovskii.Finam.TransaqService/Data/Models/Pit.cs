﻿namespace Gordovskii.Finam.TransaqService.Data
{
    public class Pit
    {
        public string SecurityCode { get; set; }
        public string BoardId { get; set; }
        public int Decimals { get; set; }
        public double MinimumStep { get; set; }
        public int LotSize { get; set; }
        public double PointCost { get; set; }
    }

}
