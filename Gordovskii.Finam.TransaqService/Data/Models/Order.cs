﻿namespace Gordovskii.Finam.TransaqService.Data
{
    public class Order
    {
        public string Id { get; set; }
        public string SecurityId { get; set; }
        public Security Security { get; set; }
        public string ClientId { get; set; }
        public Client Client { get; set; }
        public OrderStatus Status { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public int HiddenQuantity { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime? CanceledAt { get; set; }
    }
}
