﻿namespace Gordovskii.Finam.TransaqService.Data
{
    public class CandleKind
    {
        public string Id { get; set; }
        public TimeSpan Period { get; set; }
        public string Name { get; set; }
    }
}
