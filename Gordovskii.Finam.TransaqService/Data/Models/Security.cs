﻿namespace Gordovskii.Finam.TransaqService.Data
{
    public class Security
    {
        public string Id { get; set; }
        public string BoardId { get; set; }
        public Board Board { get; set; }
        public bool Active { get; set; }
        public string Code { get; set; }
        public string ShortName { get; set; }
        public int Decimals { get; set; }
        public double MinimumStep { get; set; }
        public int LotSize { get; set; }
        public double PointCost { get; set; }
        public SecurityType Type { get; set; }
    }

    public enum SecurityType
    { 
        Undefined = 0,
        Share,
        Bond,
        Fut,
        Opt,
        Gko,
        Fob,
        Idx,
        Quotes,
        Currency,
        Adr,
        Nyse,
        Metal,
        Oil,
    }

}
