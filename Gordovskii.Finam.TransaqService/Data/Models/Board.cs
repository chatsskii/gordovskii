﻿namespace Gordovskii.Finam.TransaqService.Data
{
    public class Board
    {
        public string Id { get; set; }
        public string MarketId { get; set; }
        public Market Market { get; set; }
        public string Name { get; set; }
        public BoardTradeType Type { get; set; }
    }
}
