﻿namespace Gordovskii.Finam.TransaqService.Data
{
    public enum OrderStatus
    {
        Undefined = 0,
        Active,
        Cancelled,
        Denied,
        Disabled,
        Expired,
        Failed,
        Matched,
        Rejected,
    }
}
