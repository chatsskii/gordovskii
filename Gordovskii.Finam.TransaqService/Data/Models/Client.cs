﻿namespace Gordovskii.Finam.TransaqService.Data
{
    public class Client
    {
        public string Id { get; set; }
        public bool Removed { get; set; }
        public ClientType ClientType { get; set; }
        public string MarketId { get; set; }
        public Market Market { get; set; }
        public string Union { get; set; }
    }
}
