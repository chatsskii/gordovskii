﻿namespace Gordovskii.Finam.TransaqService.Data
{
    public sealed class Quotation
    {
        public string SecurityId { get; set; } = string.Empty;
        public double? AccuredInterest { get; set; }
        public double? OpenPrice { get; set; }
        public double? BestBid { get; set; }
        public double? BestOffer { get; set; }
        public int? TradesCount { get; set; }
        public int? TradesVolume { get; set; }
        public int? OpenInterest { get; set; }
        public int? OpenInterestDelta { get; set; }
        public double? LastTradePrice { get; set; }
        public int? LastTradeQuantity { get; set; }
        public DateTime? LastTradeTime { get; set; }
        public double? LastTradePriceDelta { get; set; }
        public double? HighPrice { get; set; }
        public double? LowPrice { get; set; }
        public double? ClosePrice { get; set; }
    }
}
