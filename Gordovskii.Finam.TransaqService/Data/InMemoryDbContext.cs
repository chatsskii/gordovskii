﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace Gordovskii.Finam.TransaqService.Data
{
    public sealed class InMemoryDbContext : TransaqDbContext
    {
        private readonly SqliteConnection _connection;
        private readonly ILoggerFactory _loggerFactory;

        public InMemoryDbContext(SqliteConnection connection, ILoggerFactory loggerFactory)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
            _loggerFactory = loggerFactory ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseSqlite(_connection);
                //.UseLoggerFactory(_loggerFactory)
                //.EnableSensitiveDataLogging();
        }
    }
}
