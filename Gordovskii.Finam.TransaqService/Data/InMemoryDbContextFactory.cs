﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace Gordovskii.Finam.TransaqService.Data
{
    public sealed class InMemoryDbContextFactory : IDbContextFactory<TransaqDbContext>, IAsyncDisposable
    {
        private readonly SqliteConnection _connection;
        private readonly ILoggerFactory _loggerFactory;

        private SqliteConnection Connection
        {
            get
            {
                EnsureConnectionOpened();
                return _connection;
            }
        }
        public InMemoryDbContextFactory(ILoggerFactory loggerFactory)
        {
            _connection = new SqliteConnection("DataSource=:memory:");
            _loggerFactory = loggerFactory ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public TransaqDbContext CreateDbContext() => new InMemoryDbContext(Connection, _loggerFactory);
        public async ValueTask DisposeAsync()
        {
            await _connection.CloseAsync().ConfigureAwait(false);
            await _connection.DisposeAsync().ConfigureAwait(false);
        }

        private void EnsureConnectionOpened()
        {
            if (_connection.State == System.Data.ConnectionState.Closed)
                _connection.Open();
        }
    }
}
