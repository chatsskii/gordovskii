﻿using Microsoft.EntityFrameworkCore;

namespace Gordovskii.Finam.TransaqService.Data
{
    public class TransaqDbContext : DbContext
    {
        public DbSet<Market> Markets { get; set; }
        public DbSet<Board> Boards { get; set; }
        public DbSet<CandleKind> CandleKinds { get; set; }
        public DbSet<Security> Securities { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}
