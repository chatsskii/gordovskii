﻿namespace Gordovskii.Finam.TransaqService
{
    internal sealed class ConnectionMonitor : IDisposable
    {
        public event EventHandler<bool>? ConnectedChanged;

        private readonly CallbackHandler _callbackHandler;
        private bool _connected = false;

        public bool Connected
        {
            get => _connected;
            set
            {
                if (_connected == value)
                    return;

                _connected = value;
                OnConnectedChanged(_connected);
            }
        }

        public ConnectionMonitor(CallbackHandler callbackHandler)
        {
            _callbackHandler = callbackHandler ?? throw new ArgumentNullException(nameof(callbackHandler));
            _callbackHandler.ConnectedReceived += _callbackHandler_ConnectedRecieved;
        }

        public void Dispose() => _callbackHandler.ConnectedReceived -= _callbackHandler_ConnectedRecieved;

        private void _callbackHandler_ConnectedRecieved(object? sender, bool e) => Connected = e;

        private void OnConnectedChanged(bool connected) => ConnectedChanged?.Invoke(this, connected);
    }
}
