﻿using Gordovskii.Trading.Client;
using System;
using System.Threading.Tasks;

namespace Gordovskii.Trading.Bot
{
    public abstract class TradingBot
    {
        protected readonly ITradingClient _client;

        protected TradingBot(ITradingClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public abstract void Start();
        public abstract Task ShutdownAsync();
    }
}
