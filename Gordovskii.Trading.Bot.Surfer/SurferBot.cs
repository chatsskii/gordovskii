﻿using Gordovskii.Trading.Client;
using Gordovskii.Trading.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gordovskii.Trading.Bot.Surfer
{
    public sealed partial class SurferBot : TradingBot
    {
        private State _state;

        public string InstrumentId { get; }
        public double PriceStep { get; } = 0.2;
        public double LossPerLot { get; } = 0.09;
        

        public SurferBot(ITradingClient client) : base(client)
        {
            _state = new ObservationState(this);
        }

        public override void Start() => _state.OnEnteredAsync();
        public override Task ShutdownAsync() => _state.OnLeavedAsync();

        private void SetState(State state)
        {
            if (state == null)
                throw new ArgumentNullException(nameof(state));

            _state.OnLeavedAsync();
            _state = state;
            _state.OnEnteredAsync();
        }

        private abstract class State
        {
            protected readonly SurferBot _bot;

            protected State(SurferBot bot)
            {
                _bot = bot ?? throw new ArgumentNullException(nameof(bot));
            }

            public virtual Task OnEnteredAsync() => Task.CompletedTask;
            public virtual Task OnLeavedAsync() => Task.CompletedTask;
        }

        private sealed class ObservationState : State
        {
            private double _observationPrice;
            private string _buyOrderId = string.Empty;
            private string _sellOrderId = string.Empty;
            private IDisposable? _updateSubscription;

            private ITradingClient Client => _bot._client;
            private string InstrumentId => _bot.InstrumentId;
            private double PriceStep => _bot.PriceStep;

            public ObservationState(SurferBot bot) : base(bot) { }

            public override async Task OnEnteredAsync()
            {
                _observationPrice = await GetCurrentPriceAsync();

                var createBuyOrderTask = Client.Orders.CreateStopAsync(new CreateStopRequest() 
                { 
                    InstrumentId = InstrumentId,
                    StopPrice = _observationPrice * (1 + PriceStep),
                    StopType = StopType.StopLoss,
                    Quantity = 1,
                    MarginAvailable = false,
                });

                var createSellOrderTask = Client.Orders.CreateStopAsync(new CreateStopRequest() 
                { 
                    InstrumentId = InstrumentId,
                    StopPrice = _observationPrice * (1 - PriceStep),
                    StopType = StopType.StopLoss,
                    Quantity = -1,
                    MarginAvailable = false,
                });

                Task.WaitAll(createBuyOrderTask, createSellOrderTask);

                if (!createBuyOrderTask.IsCompleted || !createSellOrderTask.IsCompleted)
                    throw new Exception();

                _buyOrderId = createBuyOrderTask.Result.StopOrderId;
                _sellOrderId = createSellOrderTask.Result.StopOrderId;

                _updateSubscription = Client.Orders.SubscribeToOrderUpdate(OnOrderUpdateAction);
            }
            public override Task OnLeavedAsync()
            {
                _updateSubscription?.Dispose();
                return Task.CompletedTask;
            }

            private Task<double> GetCurrentPriceAsync()
            {
                // TODO: Добавить логику получения цены инструмента.
                throw new NotImplementedException();
            }

            private void OnOrderUpdateAction(OrdersUpdateArgs args)
            {
                if (args.Order.Status != OrderStatus.Executed)
                    return;

                var response = Client.Orders.ListStopsAsync().GetAwaiter().GetResult();

                var buyStopOrder = response.StopOrders.Single(stop => stop.Id == _buyOrderId);
                var sellStopOrder = response.StopOrders.Single(stop => stop.Id == _sellOrderId);

                if (buyStopOrder.Status == OrderStatus.Executed && sellStopOrder.Status == OrderStatus.Executed)
                    throw new Exception();

                if (buyStopOrder.Status == OrderStatus.Executed)
                    OnBuyOrderCompleted();
                else if (sellStopOrder.Status == OrderStatus.Executed)
                    OnSellOrderCompleted();
            }

            private void OnBuyOrderCompleted()
            {
                var cancelSellTask = Client.Orders.CancelStopAsync(new CancelStopRequest() { StopOrderId = _sellOrderId, });
                cancelSellTask.GetAwaiter().GetResult();

                // TODO: Добавить переход на СОСТОЯНИЕ покупки.
                //_bot.SetState(new BuyState(_bot));
            } 
            private void OnSellOrderCompleted()
            {
                var cancellBuyTask = Client.Orders.CancelStopAsync(new CancelStopRequest() { StopOrderId = _buyOrderId, });
                cancellBuyTask.GetAwaiter().GetResult();

                // TODO: Добавить переход на СОСТОЯНИЕ продажи.
                //_bot.SetState(new SellState(_bot));
            }

        }
    }
}
