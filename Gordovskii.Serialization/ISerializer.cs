﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.Serialization
{
    public interface ISerializer
    {
        Task SerializeAsync<TValue>(Stream stream, TValue value, CancellationToken cancellationToken = default);
        Task<TValue> DeserializeAsync<TValue>(Stream stream, CancellationToken cancellationToken = default);
    }

    public interface ISerializer<TValue>
    {
        Task SerializeAsync(Stream stream, TValue value, CancellationToken cancellationToken = default);
        Task<TValue> DeserializeAsync(Stream stream, CancellationToken cancellationToken = default);
    }
}
