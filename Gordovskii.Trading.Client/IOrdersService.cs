﻿using Gordovskii.Trading.Client.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.Trading.Client
{
    public interface IOrdersService
    {
        Task<CreateOrderResponse> CreateOrderAsync(CreateOrderRequest request, CancellationToken cancellationToken = default);
        Task<ListOrdersResponse> ListOrdersAsync(CancellationToken cancellationToken = default);
        Task<CancelOrderResponse> CancelOrderAsync(CancelOrderRequest request, CancellationToken cancellationToken = default);

        Task<CreateStopResponse> CreateStopAsync(CreateStopRequest request, CancellationToken cancellationToken = default);
        Task<ListStopsResponse> ListStopsAsync(CancellationToken cancellationToken = default);
        Task<CancelStopResponse> CancelStopAsync(CancelStopRequest request, CancellationToken cancellationToken = default);

        IDisposable SubscribeToOrderUpdate(Action<OrdersUpdateArgs> action);
    }

    public sealed class CreateOrderRequest
    {
        public string InstrumentId { get; set; } = string.Empty;
        public int Quantity { get; set; }
        public double? Price { get; set; } = null;
        public bool MarginAvailable { get; set; }
    }
    public sealed class CreateOrderResponse
    {
        public string OrderId { get; set; }
        public DateTime ExecutionTime { get; set; }
    }

    public class CancelOrderRequest
    {
        public string OrderId { get; set; } = string.Empty;
    }
    public class CancelOrderResponse
    {
        public DateTime ExecutionTime { get; set; }
    }

    public sealed class ListOrdersResponse
    {
        public IEnumerable<Order> Orders { get; set; }
        public DateTime ExecutionTime { get; set; }
    }

    public class CreateStopRequest
    {
        public string InstrumentId { get; set; } = string.Empty;
        public int Quantity { get; set; }
        public double StopPrice { get; set; }
        public double? Price { get; set; } = null;
        public StopType StopType { get; set; }
        public bool MarginAvailable { get; set; }
    }
    public class CreateStopResponse
    {
        public string StopOrderId { get; set; }
        public DateTime ExecutionTime { get; set; }
    }

    public class ListStopsResponse
    {
        public IEnumerable<StopOrder> StopOrders { get; set; }
        public DateTime ExecutionTime { get; set; }
    }

    public class CancelStopRequest
    {
        public string StopOrderId { get; set; } = string.Empty;
    }
    public class CancelStopResponse
    {
        public DateTime ExecutionTime { get; set; }
    }

    public sealed class OrdersUpdateArgs
    {
        public DateTime Time { get; set; }
        public Order Order { get; set; }
    }
}
