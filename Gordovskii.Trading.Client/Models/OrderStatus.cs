﻿namespace Gordovskii.Trading.Client.Models
{
    public enum OrderStatus
    {
        Undefiened = 0,
        Active = 1,
        Executed = 2,
        Cancelled = 3,
    }

}
