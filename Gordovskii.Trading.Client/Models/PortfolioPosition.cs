﻿namespace Gordovskii.Trading.Client.Models
{
    public sealed class PortfolioPosition
    {
        public string InstrumentId { get; set; } = string.Empty;
        public long Quantity { get; set; }
    }
}
