﻿namespace Gordovskii.Trading.Client.Models
{
    public enum StopType
    {
        Undefiened = 0,
        StopLoss,
        TakeProfit,
    }
}
