﻿using System;

namespace Gordovskii.Trading.Client.Models
{
    public sealed class StopOrder
    {
        public string Id { get; set; } = string.Empty;
        public string InstrumentId { get; set; } = string.Empty;
        public DateTime CreatedAt { get; set; }
        public DateTime? CompletedAt { get; set; } = null;
        public DateTime? Expiration { get; set; }
        public OrderStatus Status { get; set; }
        public int Quantity { get; set; }
        public double StopPrice { get; set; }
        public double? Price { get; set; } = null;
        public StopType Type { get; set; }
    }

}
