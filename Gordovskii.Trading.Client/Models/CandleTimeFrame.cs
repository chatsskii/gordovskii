﻿namespace Gordovskii.Trading.Client.Models
{
    public enum CandleTimeframe
    {
        Undefined = 0,
        Minute,
        Minutes_5,
        Minutes_15,
        Hour,
        Day,
        Week,
    }
}
