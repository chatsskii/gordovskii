﻿using System;

namespace Gordovskii.Trading.Client.Models
{
    public sealed class Order
    {
        public string Id { get; set; } = string.Empty;
        public string InstrumentId { get; set; } = string.Empty;
        public DateTime Created { get; set; }
        public DateTime? Completed { get; set; } = null;
        public DateTime? Expiration { get; set; }
        public OrderStatus Status { get; set; }
        public int Quantity { get; set; }
        public double? Price { get; set; } = null;
    }
}
