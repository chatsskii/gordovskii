﻿using Gordovskii.Trading.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.Trading.Client
{
    public interface IPortfoliosService
    {
        Task<GetPositionsResponse> GetPositionsAsync(CancellationToken cancellationToken = default);
        Task<GetCurrenciesResponse> GetCurrenciesAsync(CancellationToken cancellationToken = default);
    }

    public sealed class ListPortfoliosResponse
    {
        public IEnumerable<string> Portfolios { get; set; } = Enumerable.Empty<string>();
    }

    public sealed class GetPositionsResponse
    {
        public IEnumerable<PortfolioPosition> Positions { get; set; } = Enumerable.Empty<PortfolioPosition>();
    }

    public class GetCurrenciesResponse
    {
        public IEnumerable<PortfolioPosition> Positions { get; set; } = Enumerable.Empty<PortfolioPosition>();
    }
}
