﻿using System;

namespace Gordovskii.Trading.Client
{
    public interface ITradingClient : IDisposable
    {
        IOrdersService Orders { get; }
        IInstrumentsService Instruments { get; }
        IPortfoliosService Portfolios { get; }
    }
}
