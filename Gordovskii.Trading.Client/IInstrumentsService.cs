﻿using Gordovskii.Trading.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.Trading.Client
{
    public interface IInstrumentsService
    {
        IDisposable SubscribeToOrderBookChanged(string instrumentId, Action<OrderBookChangedArgs> action);
        IDisposable SubscribeToPriceChanged(string instrumentId, Action<PriceChangedArgs> action);
        Task<GetCandlesResponse> GetCandlesAsync(GetCandlesRequest request, CancellationToken cancellationToken = default);
    }

    public class GetCandlesRequest
    {
        public string InstrumentId { get; set; } = string.Empty;
        public CandleTimeframe CandleTimeFrame { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }

    public class GetCandlesResponse
    {
        public IEnumerable<Candle> Candles { get; set; } = Enumerable.Empty<Candle>();
    }

    public class PriceChangedArgs
    {
        public double Price { get; set; }
    }
    public class OrderBookChangedArgs
    {
        public Dictionary<double, long> Ask { get; set; } = new Dictionary<double, long>();
        public Dictionary<double, long> Bid { get; set; } = new Dictionary<double, long>();
    }
}
