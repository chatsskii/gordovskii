﻿using Finam.TradeApi.Proto.V1;
using Grpc.Core;

namespace Gordovskii.Trading.Client.Finam
{
    internal sealed class EventSubscriptionData
    {
        public AsyncDuplexStreamingCall<SubscriptionRequest, Event> StreamingCall { get; }
        public string SubscriptionId { get; }
        public CancellationTokenSource TokenSource { get; }

        public EventSubscriptionData(AsyncDuplexStreamingCall<SubscriptionRequest, Event> streamingCall, string subscriptionId, CancellationTokenSource tokenSource)
        {
            StreamingCall = streamingCall ?? throw new ArgumentNullException(nameof(streamingCall));
            SubscriptionId = subscriptionId ?? throw new ArgumentNullException(nameof(subscriptionId));
            TokenSource = tokenSource ?? throw new ArgumentNullException(nameof(tokenSource));
        }
    }
}
