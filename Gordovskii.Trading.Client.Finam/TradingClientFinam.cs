﻿using Grpc.Core.Interceptors;

namespace Gordovskii.Trading.Client.Finam
{
    public sealed partial class TradingClientFinam : ITradingClient
    {
        private readonly FinamChannel _channel;
        private readonly OrdersService _orders;
        private readonly InstrumentsService _instruments;
        private readonly PortfoliosService _portfolios;

        public string ClientId { get; }
        public string Board { get; }
        public TimeSpan PricePoolingDelay { get; set; } = TimeSpan.FromSeconds(30);

        public IOrdersService Orders => _orders;
        public IInstrumentsService Instruments => _instruments;
        public IPortfoliosService Portfolios => _portfolios;

        public TradingClientFinam(FinamChannel channel, string clientId, string board, ITokenSource tokenSource)
        {
            if (string.IsNullOrEmpty(clientId))
                throw new ArgumentNullException(nameof(clientId));

            if (string.IsNullOrEmpty(board))
                throw new ArgumentNullException(nameof(board));

            ClientId = clientId;
            Board = board;
            _channel = channel ?? throw new ArgumentNullException(nameof(channel));

            var callInvoker = _channel.Channel.Intercept(new AuthorizationInterceptor(tokenSource));
            _orders = new OrdersService(this, callInvoker);
            _instruments = new InstrumentsService(this, callInvoker);
            _portfolios = new PortfoliosService(this, callInvoker);
        }

        public void Dispose()
        {
            _orders.Dispose();
            _instruments.Dispose();
        }
    }
}
