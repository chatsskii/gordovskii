﻿using Finam.TradeApi.Grpc.V1;
using Finam.TradeApi.Proto.V1;
using Google.Protobuf.WellKnownTypes;
using Google.Type;
using Gordovskii.Trading.Client.Finam.Conversion;
using Gordovskii.Trading.Client.Models;
using Grpc.Core;
using IntradayCandleTimeFrame = Finam.TradeApi.Proto.V1.IntradayCandleTimeFrame;

namespace Gordovskii.Trading.Client.Finam
{
    public sealed partial class TradingClientFinam
    {
        private sealed class InstrumentsService : IInstrumentsService, IDisposable
        {
            private readonly TradingClientFinam _client;
            private readonly Candles.CandlesClient _candlesClient;
            private readonly Securities.SecuritiesClient _securitiesClient;

            private string Board => _client.Board;
            public TimeSpan PricePoolingDelay => _client.PricePoolingDelay;

            public InstrumentsService(TradingClientFinam client, CallInvoker callInvoker)
            {
                if (callInvoker == null)
                    throw new ArgumentNullException(nameof(callInvoker));

                _client = client ?? throw new ArgumentNullException(nameof(client));
                _candlesClient = new Candles.CandlesClient(callInvoker);
                _securitiesClient = new Securities.SecuritiesClient(callInvoker);
            }
            public async Task<GetCandlesResponse> GetCandlesAsync(GetCandlesRequest request, CancellationToken cancellationToken = default)
            {
                if (string.IsNullOrEmpty(request.InstrumentId))
                    throw new ArgumentNullException(nameof(request.InstrumentId));

                if (request.CandleTimeFrame == CandleTimeframe.Undefined)
                    throw new ArgumentException(nameof(request.CandleTimeFrame));

                if (request.From > request.To)
                    throw new ArgumentException(nameof(request.From));

                var intradayTimeFrame = request.CandleTimeFrame.ToIntradayTimeFrame();
                if (intradayTimeFrame != IntradayCandleTimeFrame.Unspecified)
                {
                    var grpcRequest = new GetIntradayCandlesRequest()
                    {
                        SecurityCode = request.InstrumentId,
                        SecurityBoard = Board,
                        TimeFrame = intradayTimeFrame,
                        Interval = new IntradayCandleInterval()
                        {
                            From = request.From.ToTimestamp(),
                            To = request.To.ToTimestamp(),
                        }
                    };

                    var response = await _candlesClient.GetIntradayCandlesAsync(grpcRequest, cancellationToken: cancellationToken);
                    return new GetCandlesResponse() { Candles = response.Candles.ToModel(), };
                }

                var dayTimeFrame = request.CandleTimeFrame.ToDayTimeFrame();
                if (dayTimeFrame != DayCandleTimeFrame.Unspecified)
                {
                    var grpcReques = new GetDayCandlesRequest()
                    {
                        SecurityCode = request.InstrumentId,
                        SecurityBoard = Board,
                        TimeFrame = dayTimeFrame,
                        Interval = new DayCandleInterval()
                        {
                            From = request.From.ToDate(),
                            To = request.To.ToDate(),
                        }
                    };

                    var response = await _candlesClient.GetDayCandlesAsync(grpcReques, cancellationToken: cancellationToken);
                    return new GetCandlesResponse() { Candles = response.Candles.ToModel(), };
                }

                throw new ArgumentException(nameof(request.CandleTimeFrame));
            }
            public IDisposable SubscribeToOrderBookChanged(string instrumentId, Action<OrderBookChangedArgs> action)
            {
                throw new NotImplementedException();
            }

            public IDisposable SubscribeToPriceChanged(string instrumentId, Action<PriceChangedArgs> action)
            {
                throw new NotImplementedException();
            }

            public void Dispose()
            {

            }
        }
    }
}
