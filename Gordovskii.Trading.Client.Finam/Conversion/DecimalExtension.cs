﻿using Decimal = Finam.TradeApi.Proto.V1.Decimal;

namespace Gordovskii.Trading.Client.Finam.Conversion
{
    internal static class DecimalExtension
    {
        public static double FromDecimal(this Decimal dec) => dec.Num * Math.Pow(10, -dec.Scale);
    }
}
