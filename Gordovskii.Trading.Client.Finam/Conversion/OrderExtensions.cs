﻿using Finam.TradeApi.Proto.V1;
using BuySell = Finam.TradeApi.Proto.V1.BuySell;
using OrderGrpc = Finam.TradeApi.Proto.V1.Order;
using OrderModel = Gordovskii.Trading.Client.Models.Order;
using OrderValidBeforeType = Finam.TradeApi.Proto.V1.OrderValidBeforeType;
using StatusGrpc = Finam.TradeApi.Proto.V1.OrderStatus;
using StatusModel = Gordovskii.Trading.Client.Models.OrderStatus;

namespace Gordovskii.Trading.Client.Finam
{
    internal static class OrderExtensions
    {
        public static OrderModel ToModel(this OrderGrpc order) => new OrderModel()
        {
            Id = order.TransactionId.ToString(),
            Status = order.Status.ToModel(),
            InstrumentId = order.SecurityCode,
            Price = order.Price,
            Quantity = (order.BuySell == BuySell.Buy) ? order.Quantity : -order.Quantity,
            Created = order.CreatedAt.ToDateTime(),
            Completed = order.AcceptedAt.ToDateTime(),
            Expiration = (order.ValidBefore.Type == OrderValidBeforeType.ExactTime || order.ValidBefore.Type == OrderValidBeforeType.TillEndSession) ? order.ValidBefore.Time.ToDateTime() : null,
        };

        public static OrderModel ToModel(this OrderEvent order) => new OrderModel()
        {
            Id = order.TransactionId.ToString(),
            Status = order.Status.ToModel(),
            InstrumentId = order.SecurityCode,
            Price = order.Price,
            Quantity = (order.BuySell == BuySell.Buy) ? order.Quantity : -order.Quantity,
            Created = order.CreatedAt.ToDateTime(),
            Completed = order.AcceptedAt.ToDateTime(),
            Expiration = (order.ValidBefore.Type == OrderValidBeforeType.ExactTime || order.ValidBefore.Type == OrderValidBeforeType.TillEndSession) ? order.ValidBefore.Time.ToDateTime() : null,
        };

        public static IEnumerable<OrderModel> ToModel(this IEnumerable<OrderGrpc> orders)
            => orders.Select(order => order.ToModel());

        public static IEnumerable<OrderModel> ToModel(this IEnumerable<OrderEvent> orderEvents)
            => orderEvents.Select(orderEvent => orderEvent.ToModel());

        public static StatusModel ToModel(this StatusGrpc status)
        {
            switch (status)
            {
                case StatusGrpc.Active:
                return StatusModel.Active;

                case StatusGrpc.Cancelled:
                return StatusModel.Cancelled;

                case StatusGrpc.Matched:
                return StatusModel.Executed;

                default:
                return StatusModel.Undefiened;
            }
        }
    }
}
