﻿using Finam.TradeApi.Proto.V1;
using Gordovskii.Trading.Client.Models;

namespace Gordovskii.Trading.Client.Finam.Conversion
{
    internal static class CandlesExtensions
    {
        public static IntradayCandleTimeFrame ToIntradayTimeFrame(this CandleTimeframe model)
        {
            switch (model)
            {
                case CandleTimeframe.Minute:
                    return IntradayCandleTimeFrame.M1;

                case CandleTimeframe.Minutes_5:
                    return IntradayCandleTimeFrame.M5;

                case CandleTimeframe.Minutes_15:
                    return IntradayCandleTimeFrame.M15;

                case CandleTimeframe.Hour:
                    return IntradayCandleTimeFrame.H1;

                default:
                    return IntradayCandleTimeFrame.Unspecified;
            }
        }
        public static DayCandleTimeFrame ToDayTimeFrame(this CandleTimeframe model)
        {
            switch (model)
            {
                case CandleTimeframe.Day:
                    return DayCandleTimeFrame.D1;

                case CandleTimeframe.Week:
                    return DayCandleTimeFrame.W1;

                default:
                    return DayCandleTimeFrame.Unspecified;
            }
        }

        public static Candle ToModel(this IntradayCandle candle) => new Candle()
        {
            Date = candle.Timestamp.ToDateTime(),
            Open = candle.Open.FromDecimal(),
            Close = candle.Close.FromDecimal(),
            High = candle.High.FromDecimal(),
            Low = candle.Low.FromDecimal(),
            Volume = candle.Volume,
        };

        public static IEnumerable<Candle> ToModel(this IEnumerable<IntradayCandle> candles) 
            => candles.Select(candle => candle.ToModel());

        public static Candle ToModel(this DayCandle candle) => new Candle()
        {
            Date = candle.Date.ToDateTime(),
            Open = candle.Open.FromDecimal(),
            Close = candle.Close.FromDecimal(),
            High = candle.High.FromDecimal(),
            Low = candle.Low.FromDecimal(),
            Volume = candle.Volume,
        };

        public static IEnumerable<Candle> ToModel(this IEnumerable<DayCandle> candles)
            => candles.Select(candle => candle.ToModel());
    }

}
