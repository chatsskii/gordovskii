﻿using Finam.TradeApi.Proto.V1;
using Gordovskii.Trading.Client.Models;
using BuySell = Finam.TradeApi.Proto.V1.BuySell;
using StatusModel = Gordovskii.Trading.Client.Models.OrderStatus;

namespace Gordovskii.Trading.Client.Finam
{
    internal static class StopOderExtensions
    {
        public static IEnumerable<StopOrder> ToModel(this Stop stop)
        {
            if (stop.StopLoss != null)
            {
                var result = new StopOrder()
                {
                    Id = stop.StopId.ToString(),
                    Status = stop.Status.ToModel(),
                    InstrumentId = stop.SecurityCode,
                    CreatedAt = stop.AcceptedAt.ToDateTime(),
                    Expiration = stop.ExpirationDate?.ToDateTime(),
                    CompletedAt = (stop.Status == StopStatus.Cancelled) ? stop.CanceledAt.ToDateTime() : null,
                };

                result.Type = StopType.StopLoss;
                result.StopPrice = stop.StopLoss.ActivationPrice;
                result.Price = (stop.StopLoss.MarketPrice) ? null : stop.StopLoss.Price;

                if (stop.StopLoss.Quantity.Units == StopQuantityUnits.Lots)
                    result.Quantity = (int)stop.StopLoss.Quantity.Value;

                if (stop.BuySell == BuySell.Sell)
                    result.Quantity *= -1;

                yield return result;
            }

            if (stop.TakeProfit != null)
            {
                var result = new StopOrder()
                {
                    Id = stop.StopId.ToString(),
                    Status = stop.Status.ToModel(),
                    InstrumentId = stop.SecurityCode,
                    CreatedAt = stop.AcceptedAt.ToDateTime(),
                    Expiration = stop.ExpirationDate?.ToDateTime(),
                    CompletedAt = (stop.Status == StopStatus.Cancelled) ? stop.CanceledAt.ToDateTime() : null,
                };

                result.Type = StopType.TakeProfit;
                result.StopPrice = stop.TakeProfit.ActivationPrice;
                result.Price = (stop.TakeProfit.MarketPrice) ? null : stop.TakeProfit.ActivationPrice;

                if (stop.TakeProfit.Quantity.Units == StopQuantityUnits.Lots)
                    result.Quantity = (int)stop.TakeProfit.Quantity.Value;

                if (stop.BuySell == BuySell.Sell)
                    result.Quantity *= -1;

                yield return result;
            }
        }

        public static IEnumerable<StopOrder> ToModel(this IEnumerable<Stop> stops)
        {
            var result = new List<StopOrder>();
            foreach (var stop in stops)
                result.AddRange(stop.ToModel());

            return result;
        }

        public static StatusModel ToModel(this StopStatus status)
        {
            switch (status)
            {
                case StopStatus.Active:
                return StatusModel.Active;

                case StopStatus.Cancelled:
                return StatusModel.Cancelled;

                case StopStatus.Executed:
                return StatusModel.Executed;

                default:
                return StatusModel.Undefiened;
            }
        }
    }
}
