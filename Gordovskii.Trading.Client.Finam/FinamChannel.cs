﻿using Grpc.Net.Client;

namespace Gordovskii.Trading.Client.Finam
{
    public sealed class FinamChannel : IAsyncDisposable
    {
        private const string ApiAddress = "https://trade-api.finam.ru";
        public GrpcChannel Channel { get; }

        public FinamChannel()
        {
            Channel = GrpcChannel.ForAddress(ApiAddress);
        }

        public async ValueTask DisposeAsync()
        {
            await Channel.ShutdownAsync();
            Channel.Dispose();
        }
    }
}
