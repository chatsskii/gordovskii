﻿using Grpc.Core;
using Grpc.Core.Interceptors;

namespace Gordovskii.Trading.Client.Finam
{
    internal sealed class AuthorizationInterceptor : Interceptor
    {
        public const string AuthorizationHeader = "X-Api-Key";
        private ITokenSource _tokenSource;

        public AuthorizationInterceptor(ITokenSource tokenSource)
        {
            _tokenSource = tokenSource ?? throw new ArgumentNullException(nameof(tokenSource));
        }

        public override AsyncUnaryCall<TResponse> AsyncUnaryCall<TRequest, TResponse>(TRequest request, ClientInterceptorContext<TRequest, TResponse> context, AsyncUnaryCallContinuation<TRequest, TResponse> continuation)
        {
            context = AppendAuthorization(context);
            return base.AsyncUnaryCall(request, context, continuation);
        }

        public override AsyncClientStreamingCall<TRequest, TResponse> AsyncClientStreamingCall<TRequest, TResponse>(ClientInterceptorContext<TRequest, TResponse> context, AsyncClientStreamingCallContinuation<TRequest, TResponse> continuation)
        {
            context = AppendAuthorization(context);
            return base.AsyncClientStreamingCall(context, continuation);
        }

        private ClientInterceptorContext<TRequest, TResponse> AppendAuthorization<TRequest, TResponse>(ClientInterceptorContext<TRequest, TResponse> context)
            where TRequest : class
            where TResponse : class
        {
            var headers = new Metadata()
            {
                { AuthorizationHeader, _tokenSource.GetToken() }
            };
            var options = context.Options.WithHeaders(headers);

            return new ClientInterceptorContext<TRequest, TResponse>(context.Method, context.Host, options);
        }
    }
}
