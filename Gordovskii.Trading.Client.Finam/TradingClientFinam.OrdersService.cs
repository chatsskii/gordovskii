﻿using Finam.TradeApi.Grpc.V1;
using Finam.TradeApi.Proto.V1;
using Gordovskii.Trading.Client.Models;
using Grpc.Core;
using CancelOrderRequestFinam = Finam.TradeApi.Proto.V1.CancelOrderRequest;
using CancelStopRequestFinam = Finam.TradeApi.Proto.V1.CancelStopRequest;

namespace Gordovskii.Trading.Client.Finam
{
    public sealed partial class TradingClientFinam
    {
        private sealed class OrdersService : IOrdersService, IDisposable
        {
            private readonly TradingClientFinam _client;

            private readonly Orders.OrdersClient _ordersClient;
            private readonly Stops.StopsClient _stopsClient;
            private readonly Events.EventsClient _eventsClient;
            private readonly HashSet<Action<OrdersUpdateArgs>> _updateActions = new HashSet<Action<OrdersUpdateArgs>>();

            private EventSubscriptionData? _subscriptionData = null;
            private Task? _updateTask;

            private string ClientId => _client.ClientId;
            private string Board => _client.Board;

            public OrdersService(TradingClientFinam client, CallInvoker callInvoker)
            {
                if (callInvoker == null)
                    throw new ArgumentNullException(nameof(callInvoker));

                _client = client ?? throw new ArgumentNullException(nameof(client));
                _ordersClient = new Orders.OrdersClient(callInvoker);
                _stopsClient = new Stops.StopsClient(callInvoker);
                _eventsClient = new Events.EventsClient(callInvoker);
            }

            public async Task<CreateOrderResponse> CreateOrderAsync(CreateOrderRequest request, CancellationToken cancellationToken = default)
            {
                var grpcRequest = new NewOrderRequest()
                {
                    ClientId = ClientId,
                    Price = request.Price,
                    Quantity = Math.Abs(request.Quantity),
                    BuySell = (request.Quantity < 0) ? BuySell.Sell : BuySell.Buy,
                    SecurityCode = request.InstrumentId,
                    SecurityBoard = Board,
                    UseCredit = request.MarginAvailable,
                    Property = OrderProperty.ImmOrCancel,
                };

                try
                {
                    var response = await _ordersClient.NewOrderAsync(grpcRequest, cancellationToken: cancellationToken).ConfigureAwait(false);

                    return new CreateOrderResponse()
                    {
                        OrderId = response.TransactionId.ToString(),
                        ExecutionTime = DateTime.UtcNow,
                    };
                }
                catch (RpcException standByModeException) when (standByModeException.Status.Detail.Contains("[171]"))
                {
                    throw new InvalidOperationException(standByModeException.Status.Detail);
                }
                catch
                {
                    throw;
                }

            }
            public async Task<CancelOrderResponse> CancelOrderAsync(CancelOrderRequest request, CancellationToken cancellationToken = default)
            {
                if (!int.TryParse(request.OrderId, out int transaqtionId))
                    throw new ArgumentException(nameof(request.OrderId));

                var grpcRequest = new CancelOrderRequestFinam()
                {
                    ClientId = ClientId,
                    TransactionId = transaqtionId,
                };

                await _ordersClient.CancelOrderAsync(grpcRequest, cancellationToken: cancellationToken).ConfigureAwait(false);
                return new CancelOrderResponse() { ExecutionTime = DateTime.UtcNow };
            }
            public async Task<ListOrdersResponse> ListOrdersAsync(CancellationToken cancellationToken = default)
            {
                var grpcRequest = new GetOrdersRequest()
                {
                    ClientId = ClientId,
                    IncludeActive = true,
                    IncludeCanceled = true,
                    IncludeMatched = true,
                };

                var response = await _ordersClient.GetOrdersAsync(grpcRequest, cancellationToken: cancellationToken).ConfigureAwait(false);

                return new ListOrdersResponse()
                {
                    ExecutionTime = DateTime.UtcNow,
                    Orders = response.Orders.ToModel(),
                };
            }

            public async Task<CreateStopResponse> CreateStopAsync(CreateStopRequest request, CancellationToken cancellationToken = default)
            {
                if (request == null)
                    throw new ArgumentNullException(nameof(request));

                if (request.StopType == Models.StopType.Undefiened)
                    throw new ArgumentException(nameof(request.StopType));

                var grpcRequest = new NewStopRequest()
                {
                    ClientId = ClientId,
                    BuySell = (request.Quantity < 0) ? BuySell.Sell : BuySell.Buy,
                    SecurityCode = request.InstrumentId,
                    SecurityBoard = Board,
                };

                if (request.StopType == StopType.StopLoss)
                {
                    grpcRequest.StopLoss = new StopLoss() 
                    { 
                        ActivationPrice = request.StopPrice,
                        MarketPrice = request.Price == null,
                        Price = request.Price ?? 0,
                        UseCredit = request.MarginAvailable,
                        Quantity = new StopQuantity 
                        { 
                            Units = StopQuantityUnits.Lots, 
                            Value = request.Quantity, 
                        }
                    };
                }
                else if (request.StopType == StopType.TakeProfit)
                {
                    grpcRequest.TakeProfit = new TakeProfit()
                    {
                        ActivationPrice = request.StopPrice,
                        MarketPrice = request.Price == null,
                        UseCredit = request.MarginAvailable,
                        Quantity = new StopQuantity
                        {
                            Units = StopQuantityUnits.Lots,
                            Value = request.Quantity,
                        }
                    };
                }

                var response = await _stopsClient.NewStopAsync(grpcRequest, cancellationToken: cancellationToken).ConfigureAwait(false);

                return new CreateStopResponse()
                {
                    StopOrderId = response.StopId.ToString(),
                    ExecutionTime = DateTime.UtcNow,
                };
            }
            public async Task<CancelStopResponse> CancelStopAsync(CancelStopRequest request, CancellationToken cancellationToken = default)
            {
                if (!int.TryParse(request.StopOrderId, out int stopId))
                    throw new ArgumentException(nameof(request.StopOrderId));

                var grpcRequest = new CancelStopRequestFinam()
                {
                    ClientId = ClientId,
                    StopId = stopId,
                };

                await _stopsClient.CancelStopAsync(grpcRequest, cancellationToken: cancellationToken).ConfigureAwait(false);
                return new CancelStopResponse() { ExecutionTime = DateTime.UtcNow };
            }
            public async Task<ListStopsResponse> ListStopsAsync(CancellationToken cancellationToken = default)
            {
                var grpcRequest = new GetStopsRequest()
                {
                    ClientId = ClientId,
                    IncludeActive = true,
                    IncludeCanceled = true,
                };

                var response = await _stopsClient.GetStopsAsync(grpcRequest, cancellationToken: cancellationToken).ConfigureAwait(false);

                return new ListStopsResponse()
                {
                    ExecutionTime = DateTime.UtcNow,
                    StopOrders = response.Stops.ToModel(),
                };
            }

            public IDisposable SubscribeToOrderUpdate(Action<OrdersUpdateArgs> action) => UpdateSubscription.Create(action, this);
            public void Dispose() => EnsureUpdateStreamCancelled();

            private void AddUpdateAction(Action<OrdersUpdateArgs> action)
            {
                if (_updateActions.Count == 0)
                {
                    var subscriptionId = Guid.NewGuid().ToString();

                    var grpcRequest = new OrderTradeSubscribeRequest()
                    {
                        IncludeOrders = true,
                        IncludeTrades = true,
                        RequestId = subscriptionId,
                    };

                    var tokenSource = new CancellationTokenSource();
                    var stream = _eventsClient.GetEvents(cancellationToken: tokenSource.Token);

                    _updateTask = StartUpdate(stream.ResponseStream, tokenSource.Token);
                    stream.RequestStream.WriteAsync(new SubscriptionRequest() { OrderTradeSubscribeRequest = grpcRequest, }).ConfigureAwait(false);
                    _subscriptionData = new EventSubscriptionData(stream, subscriptionId, tokenSource);
                }

                lock (_updateActions)
                    _updateActions.Add(action);
            }
            private void RemoveUpdateAction(Action<OrdersUpdateArgs> action)
            {
                lock (_updateActions)
                    _updateActions.Remove(action);

                if (_updateActions.Count == 0)
                    EnsureUpdateStreamCancelled();
            }
            private async Task StartUpdate(IAsyncStreamReader<Event> streamReader, CancellationToken cancellationToken)
            {
                while (await streamReader.MoveNext(cancellationToken).ConfigureAwait(false))
                {
                    var order = streamReader.Current.Order;
                    if (order == null)
                        continue;

                    var args = new OrdersUpdateArgs()
                    {
                        Time = DateTime.UtcNow,
                        Order = order.ToModel(),
                    };

                    lock (_updateActions)
                    {
                        foreach (var action in _updateActions)
                            action.Invoke(args);
                    }
                }
            }
            private void EnsureUpdateStreamCancelled()
            {
                if (_subscriptionData == null)
                    return;

                if (_updateTask == null)
                    throw new ArgumentException(nameof(_updateTask));

                var grpcRequest = new OrderTradeUnsubscribeRequest()
                {
                    RequestId = _subscriptionData.SubscriptionId,
                };

                var unsubscribeTask = _subscriptionData.StreamingCall.RequestStream.WriteAsync(new SubscriptionRequest() { OrderTradeUnsubscribeRequest = grpcRequest, });
                _subscriptionData.TokenSource.Cancel();
                _subscriptionData.TokenSource.Dispose();

                _subscriptionData = null;

                Task.WaitAll(unsubscribeTask, _updateTask);
            }


            private sealed class UpdateSubscription : IDisposable
            {
                private OrdersService _service;
                private Action<OrdersUpdateArgs> _action;

                private UpdateSubscription(Action<OrdersUpdateArgs> action, OrdersService service)
                {
                    _action = action ?? throw new ArgumentNullException(nameof(action));
                    _service = service ?? throw new ArgumentNullException(nameof(service));
                    _service.AddUpdateAction(_action);
                }

                public static IDisposable Create(Action<OrdersUpdateArgs> action, OrdersService service)
                    => new UpdateSubscription(action, service);

                public void Dispose() => _service.RemoveUpdateAction(_action);
            }
        }
    }
}
