﻿using Finam.TradeApi.Grpc.V1;
using Finam.TradeApi.Proto.V1;
using Gordovskii.Trading.Client.Models;
using Grpc.Core;

namespace Gordovskii.Trading.Client.Finam
{
    public sealed partial class TradingClientFinam
    {
        private class PortfoliosService : IPortfoliosService
        {
            private readonly TradingClientFinam _client;
            private readonly Portfolios.PortfoliosClient _portfoliosClient;

            private string ClientId => _client.ClientId;

            public PortfoliosService(TradingClientFinam client, CallInvoker callInvoker)
            {
                if (callInvoker == null)
                    throw new ArgumentNullException(nameof(callInvoker));

                _client = client ?? throw new ArgumentNullException(nameof(client));
                _portfoliosClient = new Portfolios.PortfoliosClient(callInvoker);
            }

            public async Task<GetCurrenciesResponse> GetCurrenciesAsync(CancellationToken cancellationToken = default)
            {
                var finamRequest = new GetPortfolioRequest()
                {
                    ClientId = ClientId,
                    Content = new PortfolioContent()
                    {
                        IncludeCurrencies = true,
                    }
                };

                var response = await _portfoliosClient.GetPortfolioAsync(finamRequest, cancellationToken: cancellationToken).ConfigureAwait(false);

                var positions = new List<PortfolioPosition>();
                positions.AddRange(response.Currencies.Select(currency => new PortfolioPosition() { InstrumentId = currency.Name, Quantity = (long)currency.Balance, }));

                return new GetCurrenciesResponse() { Positions = positions, };
            }

            public async Task<GetPositionsResponse> GetPositionsAsync(CancellationToken cancellationToken = default)
            {
                var finamRequest = new GetPortfolioRequest()
                {
                    ClientId = ClientId,
                    Content = new PortfolioContent()
                    {
                        IncludeCurrencies = true,
                        IncludePositions = true,
                    }
                };

                var response = await _portfoliosClient.GetPortfolioAsync(finamRequest, cancellationToken: cancellationToken).ConfigureAwait(false);

                var positions = new List<PortfolioPosition>();
                positions.AddRange(response.Positions.Select(position => new PortfolioPosition() { InstrumentId = position.SecurityCode, Quantity = position.Balance, }));
                positions.AddRange(response.Currencies.Select(currency => new PortfolioPosition() { InstrumentId = currency.Name, Quantity = (long)currency.Balance, }));

                return new GetPositionsResponse() { Positions = positions, };
            }
        }
    }
}
