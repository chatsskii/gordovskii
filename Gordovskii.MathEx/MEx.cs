﻿using System;

namespace Gordovskii.MathEx
{
    public static class MEx
    {
        public const double RadToDeg = 180 / Math.PI;
        public const double DegToRad = Math.PI / 180;
        public const double PI2 = Math.PI * 2;
        public const double PId2 = Math.PI / 2;

        public static double ToDeg(this double rad) => rad * RadToDeg;
        public static double ToRad(this double deg) => deg * DegToRad;
        public static double Abs(this double value) => Math.Abs(value);
    }
}
