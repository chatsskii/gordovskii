﻿using Gordovskii.Serialization;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.PersistentData.DataProviders
{
    public sealed class SerializationDataProvider<TData> : IDataProvider<TData>
    {
        private readonly ISerializer<TData> _serializer;
        private readonly IStreamFactory _streamFactory;

        public SerializationDataProvider(ISerializer<TData> serializer, IStreamFactory streamFactory)
        {
            _serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
            _streamFactory = streamFactory ?? throw new ArgumentNullException(nameof(streamFactory));
        }

        public async Task<TData> GetAsync(CancellationToken cancellationToken = default)
        {
            using (var stream = _streamFactory.CreateReadStream())
                return await _serializer.DeserializeAsync(stream, cancellationToken).ConfigureAwait(false);
        }
        public async Task UpdateAsync(TData data, CancellationToken cancellationToken = default)
        {
            using (var stream = _streamFactory.CreateWriteStream())
                await _serializer.SerializeAsync(stream, data, cancellationToken).ConfigureAwait(false);
        }
    }
}
