﻿namespace Gordovskii.PersistentData.DataProviders
{
    public interface IDataProviderFactory<TObject>
    {
        IDataProvider<TObject> Create();
    }
}
