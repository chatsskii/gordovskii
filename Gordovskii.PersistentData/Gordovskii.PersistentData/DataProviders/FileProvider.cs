﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.PersistentData.DataProviders
{
    public abstract class FileProvider<TData> : IDataProvider<TData>
    {
        protected readonly FileInfo _file;

        protected FileProvider(FileInfo file)
        {
            _file = file ?? throw new ArgumentNullException(nameof(file));
        }

        public abstract Task<TData> GetAsync(CancellationToken cancellationToken = default);
        public abstract Task UpdateAsync(TData data, CancellationToken cancellationToken = default);
    }
}
