﻿using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.PersistentData.DataProviders
{
    public interface IDataProvider<TData>
    {
        Task<TData> GetAsync(CancellationToken cancellationToken = default);
        Task UpdateAsync(TData data, CancellationToken cancellationToken = default);
    }
}
