﻿using System;
using System.IO;

namespace Gordovskii.PersistentData
{
    public sealed class FileStreamFactory : IStreamFactory
    {
        private readonly FileInfo _fileInfo;

        public FileStreamFactory(FileInfo fileInfo)
        {
            _fileInfo = fileInfo ?? throw new ArgumentNullException(nameof(fileInfo));
        }

        public Stream CreateReadStream() => _fileInfo.Open(FileMode.Open);
        public Stream CreateWriteStream() => _fileInfo.Open(FileMode.Create);
    }
}
