﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.PersistentData.ActiveRecords
{
    public interface IActiveRecord<TEntity> where TEntity : class
    {
        event EventHandler<TEntity> EntityChanged;
        event EventHandler<TEntity> EntityUpdated;

        TEntity Entity { get; set; }
        Task GetAsync(CancellationToken cancellationToken = default);
        Task UpdateAsync(CancellationToken cancellationToken = default);
    }
}
