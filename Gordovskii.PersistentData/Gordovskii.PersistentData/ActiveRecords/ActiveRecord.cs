﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.PersistentData.ActiveRecords
{
    public abstract class ActiveRecord<TEntity> : IActiveRecord<TEntity> where TEntity : class
    {
        public event EventHandler<TEntity> EntityChanged;
        public event EventHandler<TEntity> EntityUpdated;

        private TEntity _entity;
        public TEntity Entity
        {
            get
            {
                EnsureInitialized();
                return _entity;
            }
            set
            {
                if (value == _entity)
                    return;

                _entity = value;
                OnEntityChanged(_entity);
            }
        }

        protected ActiveRecord(TEntity entity)
        {
            _entity = entity;
        }

        public async Task GetAsync(CancellationToken cancellationToken = default)
        {
            Entity = await DoGetAsync(cancellationToken).ConfigureAwait(false);
        }

        public async Task UpdateAsync(CancellationToken cancellationToken = default)
        {
            await DoUpdateAsync(_entity, cancellationToken).ConfigureAwait(false);
            OnEntityUpdated(_entity);
        }

        protected abstract Task<TEntity> DoGetAsync(CancellationToken cancellationToken = default);
        protected abstract Task DoUpdateAsync(TEntity entity, CancellationToken cancellationToken);

        private void EnsureInitialized()
        {
            if (_entity != null)
                return;

            _entity = DoGetAsync(CancellationToken.None).GetAwaiter().GetResult();
        }

        private void OnEntityChanged(TEntity entity) => EntityChanged?.Invoke(this, entity);
        private void OnEntityUpdated(TEntity entity) => EntityUpdated?.Invoke(this, entity);
    }
}
