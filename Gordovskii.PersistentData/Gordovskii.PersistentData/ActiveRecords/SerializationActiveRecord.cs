﻿using Gordovskii.Serialization;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.PersistentData.ActiveRecords
{
    public class SerializationActiveRecord<TEntity> : ActiveRecord<TEntity> where TEntity : class
    {
        private readonly ISerializer<TEntity> _serializer;
        private readonly IStreamFactory _streamFactory;

        public SerializationActiveRecord(ISerializer<TEntity> serializer, IStreamFactory streamFactory) : base(null)
        {
            _serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
            _streamFactory = streamFactory ?? throw new ArgumentNullException(nameof(streamFactory));
        }

        protected override async Task<TEntity> DoGetAsync(CancellationToken cancellationToken = default)
        {
            using (var stream = _streamFactory.CreateReadStream())
                return await _serializer.DeserializeAsync(stream, cancellationToken).ConfigureAwait(false);
        }

        protected override async Task DoUpdateAsync(TEntity entity, CancellationToken cancellationToken)
        {
            using (var stream = _streamFactory.CreateWriteStream())
                await _serializer.SerializeAsync(stream, entity, cancellationToken).ConfigureAwait(false);
        }
    }
}
