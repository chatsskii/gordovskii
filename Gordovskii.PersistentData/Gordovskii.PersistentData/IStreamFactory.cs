﻿using System.IO;

namespace Gordovskii.PersistentData
{
    public interface IStreamFactory
    {
        Stream CreateWriteStream();
        Stream CreateReadStream();
    }
}
