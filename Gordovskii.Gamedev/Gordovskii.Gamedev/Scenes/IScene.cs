﻿namespace Gordovskii.Gamedev.Scenes
{
    public interface IScene
    {
        void LeaveScene();
    }

    public interface IScene<TSceneContext> : IScene where TSceneContext : class
    {
        bool IsInitialized { get; }
        void Initialize(TSceneContext context);
    }
}
