﻿namespace Gordovskii.Gamedev.Scenes
{
    public interface ISceneManager 
    {
        void ToScene<TSceneContext>(IScene<TSceneContext> scene, TSceneContext context) where TSceneContext : class;
    }
}
