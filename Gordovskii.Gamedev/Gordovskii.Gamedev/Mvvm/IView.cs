﻿using System.ComponentModel;

namespace Gordovskii.Gamedev.Mvvm
{
    public interface IView
    {
        INotifyPropertyChanged DataContext { get; }
    }
}
