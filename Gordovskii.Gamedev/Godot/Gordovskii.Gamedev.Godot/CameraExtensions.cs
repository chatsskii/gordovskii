﻿using Godot;
using Gordovskii.Gamedev.Godot.Raycast;

namespace Gordovskii.Gamedev.Godot
{
    public static class CameraExtensions
    {
        public static bool IsOverlapped(this Camera3D camera, Vector3 position)
            => RaycastHelper.Raycast(camera.GlobalPosition, position, camera.GetWorld3D().DirectSpaceState) != null;
    }
}
