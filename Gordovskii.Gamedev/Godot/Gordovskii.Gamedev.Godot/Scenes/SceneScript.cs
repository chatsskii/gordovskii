﻿using Godot;
using Gordovskii.Gamedev.Scenes;

namespace Gordovskii.Gamedev.Godot.Scenes
{

    public abstract partial class SceneScript<TSceneContext> : Node, IScene<TSceneContext> where TSceneContext : class
    {
        protected TSceneContext? _context;
        public bool IsInitialized => _context != null;

        public TSceneContext? Scene => _context;

        public void Initialize(TSceneContext context)
        {
            if (IsInitialized)
                throw new InvalidOperationException(nameof(Initialize));

            _context = context ?? throw new ArgumentNullException(nameof(context));
            DoInitialize(_context);
        }

        public void LeaveScene()
        {
            BeforeLeaveScene();
            QueueFree();
        }

        protected virtual void DoInitialize(TSceneContext context) { }
        protected virtual void BeforeLeaveScene() { }
    }
}
