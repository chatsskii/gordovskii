﻿using Godot;
using Gordovskii.Gamedev.Scenes;

namespace Gordovskii.Gamedev.Godot.Scenes
{
    public class SceneManagerGodot : ISceneManagerGodot
    {
        private readonly Node _rootNode;

        public SceneManagerGodot(Node rootNode)
        {
            _rootNode = rootNode ?? throw new ArgumentNullException(nameof(rootNode));
        }

        public void ToScene<TScene>(PackedScene packedScene, TScene sceneContext) where TScene : class
        {
            if (packedScene == null)
                throw new ArgumentNullException(nameof(packedScene));

            if (sceneContext == null)
                throw new ArgumentNullException(nameof(sceneContext));

            var unpackedScene = packedScene.Instantiate();
            if (unpackedScene is IScene<TScene> sceneScript)
                ToScene(sceneScript, sceneContext);
            else
                throw new ArgumentException(nameof(packedScene));
        }
        public void ToScene<TSceneContext>(IScene<TSceneContext> scene, TSceneContext context) where TSceneContext : class
        {
            if (scene == null)
                throw new ArgumentNullException(nameof(scene));

            if (context == null)
                throw new ArgumentNullException(nameof(context));

            if (scene is Node sceneNode)
            {
                foreach (var child in _rootNode.GetChildren())
                    if (child is IScene leaveScene)
                        leaveScene.LeaveScene();

                scene.Initialize(context);
                _rootNode.AddChild(sceneNode);
            }
            else
                throw new ArgumentException(nameof(scene));
        }
    }
}
