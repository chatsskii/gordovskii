﻿using Godot;

namespace Gordovskii.Gamedev.Godot.Scenes
{
    public abstract class SceneUiScript<TSceneContext> : Control
    {
        protected TSceneContext? _sceneContext;
        public void Initialize(TSceneContext sceneContext)
        {
            _sceneContext = sceneContext ?? throw new ArgumentNullException(nameof(sceneContext));
            DoInitialize(_sceneContext);
        }

        protected abstract void DoInitialize(TSceneContext sceneContext);
    }
}
