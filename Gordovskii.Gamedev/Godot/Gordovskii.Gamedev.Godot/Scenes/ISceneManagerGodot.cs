﻿using Godot;
using Gordovskii.Gamedev.Scenes;

namespace Gordovskii.Gamedev.Godot.Scenes
{
    public interface ISceneManagerGodot : ISceneManager
    {
        void ToScene<TSceneContext>(PackedScene packedScene, TSceneContext sceneContext) where TSceneContext : class;
    }
}
