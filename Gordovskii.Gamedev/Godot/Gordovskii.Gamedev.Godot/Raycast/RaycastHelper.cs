﻿using Godot;

namespace Gordovskii.Gamedev.Godot.Raycast
{
    public static class RaycastHelper
    {
        public static RaycastHit3D? Raycast(Vector3 from, Vector3 to, PhysicsDirectSpaceState3D spaceState)
        {
            var query = PhysicsRayQueryParameters3D.Create(from, to);
            query.CollideWithAreas = true;

            var result = spaceState.IntersectRay(query);
            if (result.Count == 0)
                return null;

            return new RaycastHit3D((Vector3)result["position"], (Vector3)result["normal"], (Node3D)result["collider"]);
        }
    }
}
