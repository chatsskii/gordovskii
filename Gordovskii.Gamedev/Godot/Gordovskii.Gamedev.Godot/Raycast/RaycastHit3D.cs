﻿using Godot;

namespace Gordovskii.Gamedev.Godot.Raycast
{
    public sealed class RaycastHit3D
    {
        public Vector3 Position { get; }
        public Vector3 Normal { get; }
        public Node3D Collider { get; }
        public RaycastHit3D(Vector3 position, Vector3 normal, Node3D collider)
        {
            Position = position;
            Normal = normal;
            Collider = collider ?? throw new ArgumentNullException(nameof(collider));
        }
    }
}
