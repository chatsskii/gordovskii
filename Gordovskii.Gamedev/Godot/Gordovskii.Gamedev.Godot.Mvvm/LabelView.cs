﻿
using Godot;
using Gordovskii.Bindings;
using Gordovskii.Gamedev.Mvvm;
using System.ComponentModel;

namespace Gordovskii.Gamedev.Godot.Mvvm
{
    public class LabelView : Label, IView
    {
        private IDisposable? _textSubscription;

        private INotifyPropertyChanged? _dataContext = null;
        public INotifyPropertyChanged? DataContext
        {
            get => _dataContext;
            set
            {
                if (_dataContext == value)
                    return;

                if (_dataContext is IDisposable disposable)
                    disposable.Dispose();

                BeforeDataContextChanged(_dataContext);
                _dataContext = value;
                AfterDataContextChanged(_dataContext);
            }
        }

        protected virtual void BeforeDataContextChanged(INotifyPropertyChanged? dataContext)
        {
            _textSubscription?.Dispose();
        }
        protected virtual void AfterDataContextChanged(INotifyPropertyChanged? dataContext)
        {
            if (dataContext == null)
                return;

            _textSubscription = Binding.Property(nameof(Text)).ToProperty().SubscribeOneWay(dataContext, this);
        }
    }
}
