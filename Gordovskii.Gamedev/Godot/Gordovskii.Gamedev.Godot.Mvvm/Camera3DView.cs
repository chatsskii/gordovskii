﻿using Godot;
using Gordovskii.Gamedev.Mvvm;
using System.ComponentModel;

namespace Gordovskii.Gamedev.Godot.Mvvm
{
    public partial class Camera3DView : Camera3D, IView
    {
        private INotifyPropertyChanged? _dataContext = null;
        public INotifyPropertyChanged? DataContext
        {
            get => _dataContext;
            set
            {
                if (_dataContext == value)
                    return;

                if (_dataContext is IDisposable disposable)
                    disposable.Dispose();

                BeforeDataContextChanged(_dataContext);
                _dataContext = value;
                AfterDataContextChanged(_dataContext);
            }
        }

        protected virtual void BeforeDataContextChanged(INotifyPropertyChanged? dataContext) { }
        protected virtual void AfterDataContextChanged(INotifyPropertyChanged? dataContext) { }
    }
}
