﻿
using Godot;
using Gordovskii.Bindings;
using Gordovskii.Gamedev.Mvvm;
using System.ComponentModel;

namespace Gordovskii.Gamedev.Godot.Mvvm
{
    public partial class Node3DView : Node3D, IView
    {
        private IDisposable? _positionSubscription;
        private IDisposable? _rotationSubscription;

        private INotifyPropertyChanged? _dataContext = null;
        public INotifyPropertyChanged? DataContext
        {
            get => _dataContext;
            set
            {
                if (_dataContext == value)
                    return;

                if (_dataContext is IDisposable disposable)
                    disposable.Dispose();

                BeforeDataContextChanged(_dataContext);
                _dataContext = value;
                AfterDataContextChanged(_dataContext);
            }
        }

        protected virtual void BeforeDataContextChanged(INotifyPropertyChanged? dataContext)
        {
            _positionSubscription?.Dispose();
            _rotationSubscription?.Dispose();
        }
        protected virtual void AfterDataContextChanged(INotifyPropertyChanged? dataContext)
        {
            if (dataContext == null)
                return;

            _positionSubscription = Binding.Property(nameof(Position)).ToProperty().SubscribeOneWay(dataContext, this);
            _rotationSubscription = Binding.Property(nameof(Rotation)).ToProperty().SubscribeOneWay(dataContext, this);
        }

        public override void _ExitTree()
        {
            if (_dataContext is IDisposable disposable)
                disposable.Dispose();
        }
    }
}
