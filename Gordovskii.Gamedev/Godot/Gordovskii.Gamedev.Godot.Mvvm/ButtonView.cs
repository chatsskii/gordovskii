﻿
using Godot;
using Gordovskii.Bindings;
using Gordovskii.Gamedev.Mvvm;
using System.ComponentModel;
using System.Windows.Input;

namespace Gordovskii.Gamedev.Godot.Mvvm
{
    public partial class ButtonView : Button, IView
    {
        private IDisposable? _clickCommandSubscription;

        private ICommand? _clickCommand;
        private INotifyPropertyChanged? _dataContext;

        public ICommand? ClickCommand
        {
            get => _clickCommand;
            set 
            {
                if (_clickCommand == value)
                    return;

                BeforeClickCommandChanged(_clickCommand);
                _clickCommand = value;
                AfterClickCommandChanged(_clickCommand);
            }
        }
        public INotifyPropertyChanged? DataContext
        {
            get => _dataContext;
            set
            {
                if (_dataContext == value)
                    return;

                if (_dataContext is IDisposable disposable)
                    disposable.Dispose();

                BeforeDataContextChanged(_dataContext);
                _dataContext = value;
                AfterDataContextChanged(_dataContext);
            }
        }

        protected void DoPress() => ClickCommand?.Execute(_dataContext);

        protected virtual void BeforeDataContextChanged(INotifyPropertyChanged? dataContext)
        {
            _clickCommandSubscription?.Dispose();
        }
        protected virtual void AfterDataContextChanged(INotifyPropertyChanged? dataContext)
        {
            if (dataContext == null)
                return;

            _clickCommandSubscription = Binding.Property(nameof(ClickCommand)).ToProperty().SubscribeOneWay(dataContext, this);
        }

        protected override void Dispose(bool disposing)
        {
            DataContext = null;
            base.Dispose(disposing);
        }

        private void BeforeClickCommandChanged(ICommand? clickCommand)
        {
            if (clickCommand == null)
                return;

            clickCommand.CanExecuteChanged -= clickCommand_CanExecuteChanged;
        }
        private void AfterClickCommandChanged(ICommand? clickCommand)
        {
            if (clickCommand == null) 
                return;

            clickCommand.CanExecuteChanged += clickCommand_CanExecuteChanged;
            Disabled = !clickCommand.CanExecute(_dataContext);
        }

        private void clickCommand_CanExecuteChanged(object? sender, EventArgs e)
        {
            if (_clickCommand == null)
                throw new ArgumentException(nameof(_clickCommand));

            Disabled = !_clickCommand.CanExecute(_dataContext);
        }
    }
}
