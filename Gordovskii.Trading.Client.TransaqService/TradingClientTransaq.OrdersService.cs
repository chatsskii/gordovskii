﻿using Gordovskii.Finam.TransaqService.Grpc;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.Trading.Client.TransaqService
{
    public sealed partial class TradingClientTransaq
    {
        private sealed class OrdersService : IOrdersService
        {
            private readonly TradingClientTransaq _client;
            private readonly OrderService.OrderServiceClient _orderServiceClient;

            public OrdersService(TradingClientTransaq client)
            {
                _client = client ?? throw new ArgumentNullException(nameof(client));
                _orderServiceClient = new OrderService.OrderServiceClient(_client._channel);
            }

            public async Task<CreateOrderResponse> CreateOrderAsync(CreateOrderRequest request, CancellationToken cancellationToken = default)
            {
                var response = await _orderServiceClient.CreateAsync(new Finam.TransaqService.Grpc.CreateOrderRequest()
                {
                    SecurityId = request.InstrumentId,
                    MarginAvailable = request.MarginAvailable,
                    Price = request.Price,
                    Quantity = request.Quantity,
                }, cancellationToken: cancellationToken);

                return new CreateOrderResponse()
                {
                    OrderId = response.OrderId,
                    ExecutionTime = DateTime.UtcNow,
                };
            }
            public async Task<ListOrdersResponse> ListOrdersAsync(CancellationToken cancellationToken = default)
            {
                var response = await _orderServiceClient.ListAsync(new Finam.TransaqService.Grpc.ListOrdersRequest() { Total = false });

                var result = new ListOrdersResponse() { ExecutionTime = DateTime.UtcNow, };
                result.Orders = response.Orders.Select(x => new Models.Order()
                {
                    Id = x.Id,
                    InstrumentId = x.SecurityId,
                    Price = x.Price,
                    Quantity = x.Quantity,
                });

                return result;
            }
            public Task<CancelOrderResponse> CancelOrderAsync(CancelOrderRequest request, CancellationToken cancellationToken = default)
                => throw new NotImplementedException();
            public Task<CancelStopResponse> CancelStopAsync(CancelStopRequest request, CancellationToken cancellationToken = default)
                => throw new NotImplementedException();
            public Task<CreateStopResponse> CreateStopAsync(CreateStopRequest request, CancellationToken cancellationToken = default)
                => throw new NotImplementedException();
            public Task<ListStopsResponse> ListStopsAsync(CancellationToken cancellationToken = default) => throw new NotImplementedException();
            public IDisposable SubscribeToOrderUpdate(Action<OrdersUpdateArgs> action) => throw new NotImplementedException();
        }
    }
}
