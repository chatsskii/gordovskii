﻿using System;
using System.Collections.Generic;

namespace Gordovskii.Trading.Client.TransaqService
{
    internal sealed class PriceRecordCollection
    {
        private readonly PriceRecordFactory _priceRecordFactory;
        private readonly Dictionary<string, PriceRecord> _priceRecordsByInstrumentId = new Dictionary<string, PriceRecord>();

        public PriceRecordCollection(PriceRecordFactory priceRecordFactory)
        {
            _priceRecordFactory = priceRecordFactory ?? throw new ArgumentNullException(nameof(priceRecordFactory));
        }

        public PriceRecord EnsurePriceRecord(string instrumentId)
        {
            if (!_priceRecordsByInstrumentId.TryGetValue(instrumentId, out PriceRecord priceRecord))
            {
                priceRecord = _priceRecordFactory.Create(instrumentId);
                _priceRecordsByInstrumentId.Add(instrumentId, priceRecord);
            }

            return priceRecord;
        }
    }
}
