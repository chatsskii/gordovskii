﻿using Gordovskii.Finam.TransaqService.Grpc;
using System;

namespace Gordovskii.Trading.Client.TransaqService
{
    internal sealed class PriceRecordFactory
    {
        private readonly PriceService.PriceServiceClient _priceServiceClient;

        public PriceRecordFactory(PriceService.PriceServiceClient priceServiceClient)
        {
            _priceServiceClient = priceServiceClient ?? throw new ArgumentNullException(nameof(priceServiceClient));
        }

        public PriceRecord Create(string instrumentId) => new PriceRecord(instrumentId, _priceServiceClient);
    }
}
