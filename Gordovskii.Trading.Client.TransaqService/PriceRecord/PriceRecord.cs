﻿using Gordovskii.Finam.TransaqService.Grpc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.Trading.Client.TransaqService
{
    internal sealed class PriceRecord : IDisposable
    {
        private readonly string _instrumentId;
        private readonly PriceService.PriceServiceClient _priceServiceClient;
        private readonly List<Action> _valueChangedActions = new List<Action>();
        private readonly object _valueChangedActionsLock = new object();

        private Grpc.Core.AsyncServerStreamingCall<PriceStreamResponse> _stream;
        private CancellationTokenSource _cancellationTokenSource;
        private Task _task;

        private float _value = 0;
        public float Value
        {
            get => _value;
            private set
            {
                if (_value == value)
                    return;

                _value = value;
                InvokeValueChangedActions();
            }
        }

        private CancellationTokenSource CancellationTokenSource
        {
            get
            {
                _cancellationTokenSource?.Cancel();
                _cancellationTokenSource?.Dispose();

                _cancellationTokenSource = new CancellationTokenSource();
                return _cancellationTokenSource;
            }
        }

        public PriceRecord(string instrumentId, PriceService.PriceServiceClient priceServiceClient)
        {
            _instrumentId = instrumentId ?? throw new ArgumentNullException(nameof(instrumentId));
            _priceServiceClient = priceServiceClient ?? throw new ArgumentNullException(nameof(priceServiceClient));
        }

        public IDisposable SubscribeToValueChanged(Action action) => PriceSubscription.Subscribe(this, action);
        public void Dispose()
        {
            CancellationTokenSource?.Cancel();
            CancellationTokenSource?.Dispose();
            _stream?.Dispose();
            _task?.GetAwaiter().GetResult();
        }

        private void AddAction(Action action)
        {
            lock (_valueChangedActionsLock)
            {
                if (_valueChangedActions.Count == 0)
                {
                    _stream = _priceServiceClient.PriceStream(new PriceStreamRequest() { SecurityId = _instrumentId });
                    _task = UpdateValueAsync(CancellationTokenSource.Token);
                }

                _valueChangedActions.Add(action);
            }
        }
        private void RemoveAction(Action action)
        {
            lock (_valueChangedActions)
            {
                _valueChangedActions.Remove(action);

                if (_valueChangedActions.Count == 0)
                {
                    _cancellationTokenSource?.Cancel();
                    _cancellationTokenSource?.Dispose();
                    _cancellationTokenSource = null;

                    _stream.Dispose();
                }
            }
        }
        private void InvokeValueChangedActions()
        {
            lock (_valueChangedActionsLock)
            {
                foreach (var action in _valueChangedActions)
                    action.Invoke();
            }
        }
        private async Task UpdateValueAsync(CancellationToken cancellationToken)
        {
            while (await _stream.ResponseStream.MoveNext(cancellationToken))
                Value = _stream.ResponseStream.Current.Price;
        }

        private sealed class PriceSubscription : IDisposable
        {
            private readonly PriceRecord _priceRecord;
            private readonly Action _action;

            private bool _disposed;

            private PriceSubscription(PriceRecord priceRecord, Action action)
            {
                _priceRecord = priceRecord ?? throw new ArgumentNullException(nameof(priceRecord));
                _action = action ?? throw new ArgumentNullException(nameof(action));
                _priceRecord.AddAction(action);
            }

            public static PriceSubscription Subscribe(PriceRecord priceRecord, Action action)
                => new PriceSubscription(priceRecord, action);

            public void Dispose()
            {
                if (_disposed)
                    throw new ObjectDisposedException(nameof(PriceSubscription));

                _priceRecord.RemoveAction(_action);
                _disposed = true;
            }
        }
    }
}
