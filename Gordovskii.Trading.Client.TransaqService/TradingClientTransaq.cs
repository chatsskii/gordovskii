﻿using Grpc.Net.Client;

namespace Gordovskii.Trading.Client.TransaqService
{
    public sealed partial class TradingClientTransaq : ITradingClient
    {
        private readonly GrpcChannel _channel;

        private readonly OrdersService _orders;
        private readonly InstrumentsService _instruments;

        public TradingClientTransaq(string address)
        {
            _channel = GrpcChannel.ForAddress(address);

            _orders = new OrdersService(this);
            _instruments = new InstrumentsService(this);
        }

        public IOrdersService Orders => _orders;
        public IInstrumentsService Instruments => _instruments;

        public IPortfoliosService Portfolios => throw new System.NotImplementedException();

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
    }
}
