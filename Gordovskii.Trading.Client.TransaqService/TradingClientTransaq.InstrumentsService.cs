﻿using Gordovskii.Finam.TransaqService.Grpc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.Trading.Client.TransaqService
{
    public sealed partial class TradingClientTransaq
    {
        private sealed class InstrumentsService : IInstrumentsService
        {
            private readonly TradingClientTransaq _client;
            private readonly PriceService.PriceServiceClient _priceServiceClient;
            private readonly PriceRecordCollection _priceRecordCollection;

            public InstrumentsService(TradingClientTransaq client)
            {
                _client = client ?? throw new ArgumentNullException(nameof(client));
                _priceServiceClient = new PriceService.PriceServiceClient(_client._channel);
                _priceRecordCollection = new PriceRecordCollection(new PriceRecordFactory(_priceServiceClient));
            }

            public Task<GetCandlesResponse> GetCandlesAsync(GetCandlesRequest request, CancellationToken cancellationToken = default)
            {
                throw new NotImplementedException();
            }

            public IDisposable SubscribeToOrderBookChanged(string instrumentId, Action<OrderBookChangedArgs> action)
            {
                throw new NotImplementedException();
            }

            public IDisposable SubscribeToPriceChanged(string instrumentId, Action<PriceChangedArgs> action)
            {
                var priceRecord = _priceRecordCollection.EnsurePriceRecord(instrumentId);
                return priceRecord.SubscribeToValueChanged(() => action.Invoke(new PriceChangedArgs() { Price = priceRecord.Value }));
            }
        }
    }
}
