﻿using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.Trading
{
    public sealed class TokenSource : ITokenSource
    {
        public string Token { get; set; }

        public string GetToken() => Token;
        public Task<string> GetTokenAsync(CancellationToken cancellationToken = default) => Task.FromResult(Token);
    }
}
