﻿using System.Threading;
using System.Threading.Tasks;

namespace Gordovskii.Trading
{
    public interface ITokenSource
    {
        string GetToken();
        Task<string> GetTokenAsync(CancellationToken cancellationToken = default);
    }
}
